﻿using System;
using System.Diagnostics;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.IO.Ports;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Google.Protobuf;
using kRPC.RPC.Protocol;

namespace test {
	public class Program {

		public static TimeSpan frequence = TimeSpan.FromMilliseconds(80);
		public static DateTime birthday = new DateTime(1997, 12, 17);

		public static StreamWriter streamWriter = File.CreateText("Benchmark.txt");

		public static byte[] VelocityRPCRequest(int count) {
			var request = new Request {
				Identifier = (ulong) count,
				Procedure = new Request.Types.Procedure {
					Type = "kRPC.Services.SpaceProgram",
					Name = "ActiveVesselSpeed",
					Static = true
				}
			};
			var stream = new MemoryStream();
			var ostream = new CodedOutputStream(stream);
			ostream.WriteInt64(request.ToByteArray().Length);
			ostream.Flush();
			byte[] buffer = request.ToByteArray();
			stream.Write(buffer, 0, buffer.Length);
			return stream.ToArray();
		}

		public static string VelocityRPCResponse() => "";

		private static readonly Mutex bufferMutex = new Mutex();
		private static readonly Semaphore task = new Semaphore(0, 2);
		private static string buffer = "";

		public static void SharedMemoryTest() {
			MemoryMappedFile mapped = MemoryMappedFile.OpenExisting("kRPC_SharedMemory");
			if (mapped != null)
				Console.WriteLine("MappedMemory succesfuly opened.");

			EventWaitHandle wait = EventWaitHandle.OpenExisting("kRPC_SharedMemory_CyclicEvent");
			if (wait != null)
				Console.WriteLine("EventWaitHandle succesfuly opened.");

			using (MemoryMappedViewAccessor accessor = mapped.CreateViewAccessor(0, 0)) {
				ushort result;
				accessor.Write(0, (ushort) 0x08);
				wait.Set();
				for (var i = 1; i < 500; i++) {
					wait.WaitOne();
					accessor.Read(0x08, out result);
					Console.WriteLine("{0}: {1}", i, result);
					accessor.Write(0, (ushort) 0x08);
					wait.Set();
				}
				wait.WaitOne();
				accessor.Read(0x08, out result);
				Console.WriteLine("500: {0}", result);
			}
			Console.ReadKey();
		}

		public static void SerialTest() {
			var client = new TcpClient("127.0.0.1", 11000);
			var serial = new SerialPort {
				PortName = "COM4",
				BaudRate = 9600,
				NewLine = "\r\n"
			};
			serial.DataReceived += (sender, args) => {
				bufferMutex.WaitOne();
				buffer += serial.ReadExisting();
				if (buffer.EndsWith(serial.NewLine))
					task.Release();
				bufferMutex.ReleaseMutex();
			};
			serial.Open();
			var count = 1;
			while (count++ < 1000) {
				/* Receive */
				task.WaitOne();
				if (buffer.StartsWith("#")) {
					/* Timestamp just before writing on serial. */
					streamWriter.WriteLine((DateTime.Now - birthday).TotalMilliseconds);

					/* Send to Kerbal. */
					Console.WriteLine("Write VelocityRequest kRPC.Services.SpaceProgram.ActiveVesselVelocity");
					client.Client.Send(VelocityRPCRequest(count));

					/* Receive from Kerbal. */
					var byte_buffer = new byte[64];
					client.Client.Receive(byte_buffer);
					Console.WriteLine("Coded read: " + BitConverter.ToString(byte_buffer));
					string response = Encoding.ASCII.GetString(byte_buffer);
					response = response.Split('\0')[0];
					Console.WriteLine("Velocity: " + response);

					/* Timestamp just before writing on serial. */
					streamWriter.WriteLine((DateTime.Now - birthday).TotalMilliseconds);
					serial.WriteLine(response);
				}
			}
			Console.ReadKey();
			streamWriter.Close();
			client.Close();
			//traitement
			//preapre
			//send erbal
			//wait kerbal

			//timestamp
			//send txt microcontroller
		}

		public static void TCPTest() {
			var client = new TcpClient("127.0.0.1", 11000);
			if (client.Connected)
				Console.WriteLine("Connected.");

			var request = new Request { Identifier = 1 };
			var procedure = new Request.Types.Procedure {
				Type = "kRPC.Services.SpaceProgram",
				Name = "GameMode",
				Static = true
			};
			request.Procedure = procedure;
			var count = 1;
			while (count++ < 500) {
				DateTime start = DateTime.Now;
				var stream = new MemoryStream();

				/* Generate a request message. */
				byte[] buffer = VelocityRPCRequest(count);

				/* Append message to memory stream. */
				stream.Write(buffer, 0, buffer.Length);

				/* Timestamp 1 */
				streamWriter.WriteLine("{0}", (DateTime.Now - birthday).TotalMilliseconds);

				/* Write message on network. */
				client.Client.Send(stream.ToArray());
				Console.WriteLine("Message written.");

				var buf = new byte[5];
				client.Client.Receive(buf);

				/* Delay */
				TimeSpan time_elapsed = DateTime.Now - start;
				if (time_elapsed < frequence)
					Thread.Sleep(frequence - time_elapsed);
			}
			Console.WriteLine("Press a key to close application...");
			Console.ReadKey();
			streamWriter.Close();
			client.Client.Close();
			Console.WriteLine("Press a key to confirm...");
			Console.ReadKey();
		}

		public static void Main() => SharedMemoryTest();
	}
}
