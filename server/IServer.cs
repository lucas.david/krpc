﻿using System;

namespace kRPC {
	public interface IServer : IDisposable {

		bool IsOpen { get; }
		string LocalAddress { get; }

		event EventHandler Opened;
		event EventHandler Closed;

		void Close();

		void Open();

	}
}
