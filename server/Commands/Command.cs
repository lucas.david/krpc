﻿using System;
using System.Text.RegularExpressions;
using UnityEngine;

namespace kRPC.Commands {

	internal sealed class Command  {
		internal const string command = "krpc";

		private static readonly Regex spaces = new Regex(@"\s+");

		private static string[] ParseArguments(string argsString) {
			if (argsString == null)
				return new string[0];
			argsString = argsString.Trim();
			if (string.Empty == argsString)
				return new string[0];
			return spaces.Replace(argsString, (match) => " ").Split(' ');
		}

		public static void OnCommand(string argsString) {
			string[] args = ParseArguments(argsString);
			OnCommand(args);
		}

		internal static void OnCommand(string[] args) {
			if (args.Length == 0) {
				Debug.Log("[kRPC]: Invalid command, try \"/krpc help\" for more details.");
				return;
			}

			switch (args[0]) {
				case "-h":
				case "--help":
					if (args.Length == 1) {
						Debug.Log("[kRPC]: Usage: /krpc [command]");
						Debug.Log("For more details about a specific command use \"/krpc --help [COMMAND]\"");
						Debug.Log("Commands available: server");
						break;
					} 
					OnCommand("/krpc " + args[1]);
					break;
				case "server":
					ServerCommand.OnCommand(args);
					break;
				default:
					Debug.Log("[kRPC]: Invalid command, try \"/krpc --help\" for more details.");
					break;
			}
		}
	}
}