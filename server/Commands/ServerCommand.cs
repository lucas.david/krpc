﻿using System.Net;
using UnityEngine;

namespace kRPC.Commands {
	internal sealed class ServerCommand {
		internal static void OnCommand(string[] args) {
			if (args.Length == 1) {
				Debug.Log("[kRPC]: Available server commands: info start stop.");
				Debug.Log("Exemple: \"/krpc server --start 127.0.0.1 11000\".");
				Debug.Log("\t\"/krpc server --start shared [file_name]\".");
				Debug.Log("\t\"/krpc server --stop\".");
				return;
			}

			switch (args[1]) {
				case "-i":
				case "--info":
					if (Addon.Instance.Server != null && Addon.Instance.Server.IsOpen)
						Debug.LogFormat("[kRPC]: Server currently running, {0}.",
							Addon.Instance.Server.LocalAddress);
					else
						Debug.Log("[kRPC]: Server not currently running.");
					break;
				case "-s":
				case "--start":
					if (Addon.Instance.Server != null && Addon.Instance.Server.IsOpen) {
						Debug.LogFormat("[kRPC]: Server is already running on {0}.",
							Addon.Instance.Server.LocalAddress);
						break;
					}

					if (args.Length > 2) {
						if (args[2] == "shared") {
							Debug.LogWarning("SharedMemory not safely supported.");
							break;
							if (args.Length == 3) {
								Addon.Instance.Server = new SharedMemory.Server();
								break;
							}
							if (args.Length == 4) {
								Addon.Instance.Server = new SharedMemory.Server(args[3]); //todo
								break;
							}
							Debug.LogFormat("[kRPC]: Too many arguments for \"/krpc server -s shared [name]\".");
							break;
						}

						if (args[2].StartsWith("COM")) {
							Debug.LogWarning("IO.SerialPort not supported.");
							break; // TODO
						}

						if (IPAddress.TryParse(args[2], out IPAddress address)) {
							if (args.Length == 3) {
								Addon.Instance.Server = new RPC.Server(args[2], 0);
								Addon.Instance.Server.Open();
								break;
							}
							if (args.Length == 4) {
								if (!int.TryParse(args[3], out int port)) {
									Debug.LogWarningFormat("[kRPC]: Failed to parse port string {0}.", args[3]);
									break;
								}
								Addon.Instance.Server = new RPC.Server(args[2], port);
								Addon.Instance.Server.Open();
								break;
							}
							Debug.LogWarningFormat("[kRPC]: Too many arguments for \"/krpc server -s <address> [port]\".");
							break;
						}

						Debug.LogWarningFormat("[kRPC]: Cannot recognize first argument, \"{0}\".", args[2]);
						break;
					}
					Debug.LogWarning("[kRPC]: Not enough arguments for \"/krpc server -s <options>\".");
					break;
				case "-c":
				case "--close":
					Addon.Instance.Server.Close();
					break;
				default:
					Debug.Log("[kRPC]: Invalid command. Use \"/krpc server\" for a full list of server commands.");
					break;
			}
		}
	}
}