﻿using System.Collections.Generic;
using System.Linq;

namespace kRPC.Utils {
	public class BiDictionary<Key, Value> : Dictionary<Key, Value> {
		public Key this[Value value] {
			get {
				if (!this.Any(x => x.Value.Equals(value)))
					throw new ValueNotFoundException();
				return this.First(x => x.Value.Equals(value)).Key;
			}
		}
	}
}
