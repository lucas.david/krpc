﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace kRPC.Utils {
	public class DynamicBuffer<T> {
		public const int npos = int.MaxValue;

		private T[] _data;
		private int _size;
		private int _capacity;

		public DynamicBuffer() {
			_data = new T[0];
			_size = 0;
			_capacity = 0;
		}
		public DynamicBuffer(int size) : this() => Resize(size);
		public DynamicBuffer(int size, T fill) : this(size) {
			for (int i = 0; i < size; i++) this[i] = fill;
		}
		public DynamicBuffer(DynamicBuffer<T> other, int position) : this(other, position, other.Size()) {}
		public DynamicBuffer(DynamicBuffer<T> other, int position, int count) : this(npos == count || count > other.Size() ? other.Size() : count) {
			for (int i = 0, j = position; i < count; i++, j++) this[i] = other[j];
		}

		public T this[int index] {
			get => _data[index];
			set => _data[index] = value;
		}

		public void Append(T item) {
			if (_capacity < _size + 1) {
				Resize(_size + 1);
				_data[_size - 1] = item;
				return;
			}
			_data[_size] = item;
			_size++;
		}

		public void Append(T[] array) {
			if (_capacity < _size + array.Length)
				Resize(_size + array.Length);
			Array.ConstrainedCopy(array, 0, _data, _size, array.Length);
		}

		public void Append(DynamicBuffer<T> buffer) {
			if (_capacity < _size + buffer._size)
				Resize(_size + buffer._size);
			Array.ConstrainedCopy(buffer._data, 0, _data, _size, buffer._size);
		}

		public int Capacity() => _capacity;

		public void Clear() => Resize(0);

		public object Clone() => throw new NotImplementedException();

		public bool Contains(T item) => Array.Exists(_data, (match) => item.Equals(match));

		public void CopyTo(Array array, int index) => _data.CopyTo(array, index);

		public void CopyTo(T[] array, int index) => _data.CopyTo(array, index);

		public T[] Data() => _data;

		public void Reserve(int newCapacity) {
			Array.Resize(ref _data, newCapacity);
			_capacity = newCapacity;
		}

		public void Resize(int newSize) {
			if (_capacity < newSize) {
				for (int i = int.MaxValue; (_capacity = i & newSize) != 0 || i != 0; i >>= 1);
				Array.Resize(ref _data, _capacity);
			}
			_size = newSize;
		}

		public int Size() => _size;

		public void ShrinkToFit() {
			Reserve(_size);
		}

		public T[] ToArray() {
			T[] array = new T[_size];
			Array.Copy(_data, array, _size);
			return array;
		}
	}
}
