﻿using System;
using System.Runtime.Serialization;

namespace kRPC.Utils {
	class ValueNotFoundException : SystemException, ISerializable {
		public ValueNotFoundException() : base() {}

		public ValueNotFoundException(string message) : base(message) {}

		public ValueNotFoundException(string message, Exception innerException) : base(message, innerException) {}

		protected ValueNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context) {}
	}
}
