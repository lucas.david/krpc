﻿#define UNITY_SUBSET_NET35

using System;
using System.Collections.Generic;
using System.IO;
using kRPC.Services;
using UnityEngine;
using SlicedSharedMemory = SharedMemory.SlicedSharedMemory;

namespace kRPC.SharedMemory {
	public class Server : IServer {

		public const int active_vessel_velocity = 0x08;
		public const int active_vessel_altitude = 0x20;

		public static readonly Dictionary<string, ushort> TagOffsets = new Dictionary<string, ushort> {
			{"end", 0x00},
			{"active_vessel_velocity", 0x08}, /* offset: 8, size: 192-bits, type: double[3] */
			{"active_vessel_altitude", 0x20}, /* offset: 32, size: 64-bits, type: double */
		};

		private readonly SlicedSharedMemory _shared;

		public string LocalAddress => "MemoryMappedFile at " + _shared.MappedFileName;
		public bool IsOpen => !_shared.IsClosed();

		public event EventHandler Opened;
		public event EventHandler Closed;

		public Server() {
			throw new NotSupportedException();
			_shared = new SlicedSharedMemory("kRPC_SharedMemory", 4096, 4096);
			var offsetBuffer = new ushort[_shared.ReadSize / sizeof(ushort)];
			StreamWriter stream = File.CreateText("Benchmark");
			_shared.Received += (sender, eventsArgs) => {
				try {
					Debug.Log("[kRPC] Received signal.");
					stream.WriteLine("[Timestamp] Read (Shared Memory): {0}",
						(DateTime.Now - DateTime.Today).TotalMilliseconds);
					_shared.ReadArray(0, offsetBuffer, 0, offsetBuffer.Length);
					stream.WriteLine("[Timestamp] Write (Shared Memory): {0}",
						(DateTime.Now - DateTime.Today).TotalMilliseconds);
					for (var i = 0; i < _shared.ReadSize && offsetBuffer[i] != 0; i++) {
						Debug.LogFormat("{0}: {1}", i, offsetBuffer[i]);
						ConsumeOffset(offsetBuffer[i]);
					}
				} catch (Exception e) {
					Debug.Log(e.GetType().Name);
					Debug.Log(e.Message);
					Debug.Log(e.StackTrace);
				}
			};
			Closed += (sender, eventArgs) => Debug.Log("[kRPC]: Shared Memory has been closed.");
			Closed += (sender, eventArgs) => stream.Close();
			Opened?.Invoke(this, EventArgs.Empty);
			Debug.Log("[kRPC]: Shared Memory has been initialized successfully.");
		}

		public Server(string name) {
			throw new NotImplementedException();
		}

		private void ConsumeOffset(ushort offset) {
			try {
				switch (offset) {
					case 0:
						break;
					case active_vessel_altitude:
						double value = SpaceProgram.ActiveVesselAltitude();
						_shared.Write(active_vessel_altitude, ref value);
						break;
					default:
						throw new ArgumentOutOfRangeException(nameof(offset));
				}
			} catch (Exception e) {
				Debug.Log(e);
				throw;
			}
		}

		private void ConsumeTag(string tag) {
			if (!TagOffsets.ContainsKey(tag))
				throw new KeyNotFoundException();
			ConsumeOffset(TagOffsets[tag]);
		}

		public void Open() {

		}

		public void Close() {
			if (_shared.IsClosed())
				return;
			_shared.Close();
			Closed?.Invoke(this, EventArgs.Empty);
		}

		#region Disposable pattern

		~Server() => Dispose(false);

		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		
		protected virtual void Dispose(bool disposing) {
			if (disposing && _shared.IsClosed()) {
				_shared.Dispose();
			}
		}

		#endregion

	}
}