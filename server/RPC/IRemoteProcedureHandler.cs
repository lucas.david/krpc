﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace kRPC.RPC {
	interface IRemoteProcedureHandler {
		// Type ReturnType { get; }

		object Invoke(params object[] args);
	}
}
