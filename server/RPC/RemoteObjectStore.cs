﻿using kRPC.Utils;
using System.Collections.Generic;

namespace kRPC.RPC {
	public class RemoteObjectStore: IStore<object, RemoteObject> {
		private readonly BiDictionaryOneToOne<object, RemoteObject> _objects;
		private ulong _nextIdentifier;

		public RemoteObject this[object obj] {
			get {
				if (obj == null)
					return RemoteObject.Empty;
				if (!_objects.TryGetByFirst(obj, out RemoteObject value))
					throw new KeyNotFoundException();
				return value;
			}
		}

		public RemoteObjectStore() {
			_objects = new BiDictionaryOneToOne<object, RemoteObject>();
			_nextIdentifier = 1;
		}

		public RemoteObject Add(object obj) {
			if (obj == null)
				return RemoteObject.Empty;
			if (!_objects.TryAdd(obj, new RemoteObject {index = _nextIdentifier, value = obj}))
				return _objects.GetByFirst(obj);
			_nextIdentifier++;
			return _objects.GetByFirst(obj);
		}

		public void Clear() {
			_objects.Clear();
			_nextIdentifier = 1;
		}

		public void Remove(object obj) {
			if (obj != null)
				_objects.TryRemoveByFirst(obj);
		}
	}
}
