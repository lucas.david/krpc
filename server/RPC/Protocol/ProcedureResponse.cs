﻿using System;
using System.IO;
using kRPC.RPC.Protocol.Values;

using Object = kRPC.RPC.Protocol.Values.Object;

namespace kRPC.RPC.Protocol {
	public class ProcedureResponse : Message, IMessage<ProcedureResponse> {
		private bool _failed;
		private Value _result;
		private Error _error;

		#region Constructor

		public ProcedureResponse(uint identifier) : base(identifier, Type.Procedure | Type.Response) { }

		public ProcedureResponse(ProcedureResponse other) : base(other) {
			_failed = other._failed;
			if (_failed)
				_error = other._error.DeepClone();
			else
				_result = other._result.DeepClone();
		}

		#endregion

		/// <inheritdoc/>
		public override bool Equals(object other) => Equals(other as ProcedureResponse);

		/// <inheritdoc/>
		public override int GetHashCode() {
			unchecked {
				int hashCode = _failed.GetHashCode();
				hashCode = hashCode * 397 ^ (_result != null ? _result.GetHashCode() : 0);
				hashCode = hashCode * 397 ^ (_error != null ? _error.GetHashCode() : 0);
				return hashCode;
			}
		}

		/// <inheritdoc/>
		public override string ToString() => new JSON.Stringifier().Stringify((Value) this, "\t");


		#region Message, IMessage<ProcedureResponse> public methods

		/// <inheritdoc/>
		public override int CalculateSize() => base.CalculateSize() + 1 + (_failed ? _error.CalculateSize() : _result.CalculateSize());

		/// <inheritdoc/>
		public override Message DeepClone() => ((IDeeplyCloneable<ProcedureResponse>) this).DeepClone();

		/// <inheritdoc/>
		public override bool Equals(Message other) => Equals(other as ProcedureResponse);

		public bool Equals(ProcedureResponse other) {
			if (other is null)
				return false;
			if (ReferenceEquals(this, other))
				return true;
			return _failed == other._failed && (_failed ? _error.Equals(other._error) : _result.Equals(other._result));
		}

		/// <inheritdoc/>
		public override void Merge(Message other) => Merge(other as ProcedureResponse);

		public void Merge(ProcedureResponse other) => throw new NotSupportedException("This method has no sense here.");

		public override void ReadFrom(Stream stream) {
			base.ReadFrom(stream);
			_failed = stream.ReadBool();
			if (_failed) {
				_error = new Error();
				_error.ReadFrom(stream);
			} else {
				_result = new Value();
				_result.ReadFrom(stream);
			}
		}

		/// <inheritdoc />
		public override void WriteTo(Stream stream) {
			base.WriteTo(stream);
			stream.Write(_failed);
			if (_failed)
				_error.WriteTo(stream);
			else 
				_result.WriteTo(stream);
		}

		#endregion

		ProcedureResponse IDeeplyCloneable<ProcedureResponse>.DeepClone() => new ProcedureResponse(this);

		public static explicit operator Value(ProcedureResponse response) {
			if (response == null)
				return null;
			Object obj = new Object();
			if (response._failed)
				obj.Fields.Add("error", (Value) response._error);
			else
				obj.Fields.Add("result", response._result);
			return obj;
		}

		#if false
		private readonly IValue result;
		private readonly Error error;
		private readonly bool _isError;

		public static ProcedureResponse CreateFromValue<T>(T value) where T : unmanaged => new ProcedureResponse(new Value<T> { value
 = value });
		public static ProcedureResponse CreateFromValue<T>(T[] values) where T : unmanaged => new ProcedureResponse(new ArrayValue<T> { values
 = values });
		public static ProcedureResponse CreateFromError(Error err) => new ProcedureResponse(err);

		private ProcedureResponse(IValue value) : base(Type.Procedure | Type.Response) {
			result = value;
			error = null;
			_isError = false;
		}
		private ProcedureResponse(Error err) : base(Type.Procedure | Type.Response) {
			result = null;
			error = err;
			_isError = false;
		}
		internal ProcedureResponse() : base(Type.Procedure | Type.Response) {}

		#region IMessage

		public void ReadFrom(Stream stream) {
			if (stream.ReadByte() != 0) {
				stream.
				result.
			}
		}

		public override void ReadFrom(byte[] buffer) {

		}

		public override int ComputeSize() => _isError ? result.ComputeSize() : error.ComputeSize();

		public override string ToString() => _isError ? error.ToString() : result.ToString();

		#endregion
#endif
	}

	public class Error : IMessage<Error> {
		private string _name;
		private string _message;
		private string _stackTrace;

		public Error() { }

		public Error(Exception exception) {
			if (exception == null)
				throw new ArgumentNullException(nameof(exception));
			_name = exception.GetType().Name;
			_message = exception.Message;
			_stackTrace = exception.StackTrace;
		}

		public Error(Error other) {
			if (other == null)
				throw new ArgumentNullException(nameof(other));
			_name = (string) other._name?.Clone();
			_message = (string) other._message?.Clone();
			_stackTrace = (string) other._message?.Clone();
		}

		/// <inheritdoc />
		public override bool Equals(object other) => Equals(other as Error);

		/// <inheritdoc />
		public override int GetHashCode() {
			unchecked {
				int hashCode = _name != null ? _name.GetHashCode() : 0;
				hashCode = hashCode * 397 ^ (_message != null ? _message.GetHashCode() : 0);
				hashCode = hashCode * 397 ^ (_stackTrace != null ? _stackTrace.GetHashCode() : 0);
				return hashCode;
			}
		}

		public override string ToString() =>
			new JSON.Stringifier().Stringify((Value)this, "\t"); /* explicit conversion Error --> Value */

		#region IMessage<Error> public methods

		public int CalculateSize() {
			if (_name == null || _message == null || _stackTrace == null)
				throw new ArgumentNullException();
			return _name.CalculateSize() + _message.CalculateSize() + _stackTrace.CalculateSize();
		}

		public Error DeepClone() => new Error(this);

		/// <inheritdoc />
		public bool Equals(Error other) {
			if (other is null)
				return false;
			if (ReferenceEquals(this, other))
				return true;
			return string.Equals(_name, other._name) && string.Equals(_message, other._message) && string.Equals(_stackTrace, other._stackTrace);
		}

		public void Merge(Error other) {
			// TODO
		}

		public void ReadFrom(Stream stream) {
			if (stream == null)
				throw new ArgumentNullException(nameof(stream));
			_name = stream.ReadString();
			_message = stream.ReadString();
			_stackTrace = stream.ReadString();
		}

		public void WriteTo(Stream stream) {
			if (stream == null)
				throw new ArgumentNullException(nameof(stream));
			if (_name == null)
				throw new ArgumentException("Error should be filled with data before writing it.");
			stream.Write(_name);
			stream.Write(_message ?? "");
			stream.Write(_stackTrace ?? "");
		}

		#endregion

		public static explicit operator Error(Exception exception) => new Error(exception);

		public static explicit operator Value(Error error) {
			if (error == null)
				return null;
			Object obj = new Object();
			obj.Fields.Add("name", error._name); /* implicit conversion string - wrapped -> Value */
			obj.Fields.Add("message", error._message ?? ""); /* implicit conversion string - wrapped -> Value */
			obj.Fields.Add("stacktrace", error._stackTrace ?? ""); /* implicit conversion string - wrapped -> Value */
			return obj; /* implicit conversion Object - wrapped -> Value */
		}

	}

}