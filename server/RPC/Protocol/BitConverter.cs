﻿using System;

namespace kRPC.RPC.Protocol {

	internal class BitConverter {

		internal static byte[] GetBytes<T>(T value) where T : unmanaged {
			switch (value) {
				case bool b:
					return System.BitConverter.GetBytes(b);

				case sbyte sb:
					return System.BitConverter.GetBytes(sb);
				case short s:
					return System.BitConverter.GetBytes(s);
				case int i:
					return System.BitConverter.GetBytes(i);
				case long l:
					return System.BitConverter.GetBytes(l);

				case byte b:
					return System.BitConverter.GetBytes(b);
				case ushort us:
					return System.BitConverter.GetBytes(us);
				case uint ui:
					return System.BitConverter.GetBytes(ui);
				case ulong ul:
					return System.BitConverter.GetBytes(ul);

				case float f:
					return System.BitConverter.GetBytes(f);
				case double d:
					return System.BitConverter.GetBytes(d);

				case char c:
					return System.BitConverter.GetBytes(c);

				default:
					throw new ArgumentOutOfRangeException(nameof(value));
			}
		}


		internal static T ToTee<T>(byte[] value, int startIndex) where T : unmanaged {
			T tee = default;
			switch (tee) {
				case bool _:
					return Unsafe.As<bool, T>(System.BitConverter.ToBoolean(value, startIndex));

				case sbyte _:
					return Unsafe.As<sbyte, T>((sbyte) value[startIndex]);
				case short _:
					return Unsafe.As<short, T>(System.BitConverter.ToInt16(value, startIndex));
				case int _:
					return Unsafe.As<int, T>(System.BitConverter.ToInt32(value, startIndex));
				case long _:
					return Unsafe.As<long, T>(System.BitConverter.ToInt64(value, startIndex));

				case byte _:
					return Unsafe.As<byte, T>(value[startIndex]);
				case ushort _:
					return Unsafe.As<ushort, T>(System.BitConverter.ToUInt16(value, startIndex));
				case uint _:
					return Unsafe.As<uint, T>(System.BitConverter.ToUInt32(value, startIndex));
				case ulong _:
					return Unsafe.As<ulong, T>(System.BitConverter.ToUInt64(value, startIndex));

				case float _:
					return Unsafe.As<float, T>(System.BitConverter.ToSingle(value, startIndex));
				case double _:
					return Unsafe.As<double, T>(System.BitConverter.ToDouble(value, startIndex));

				case char c:
					return Unsafe.As<char, T>(System.BitConverter.ToChar(value, startIndex));

				default:
					throw new ArgumentOutOfRangeException(nameof(tee));
			}
		}
	}

}