﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using kRPC.RPC.Protocol.Exceptions;
using kRPC.Utils;
using KSP.UI.Screens;

namespace kRPC.RPC.Protocol {

	/**
	 * <summary>
	 *  Abstract RPC Message implementing header data, base serialization and
	 *  deserialization.
	 * </summary>
	 */
	public abstract class Message : IMessage<Message> {
		private static uint NextIdentifier = 0;

		private uint _identifier;
		private Type _type;

		protected Message(uint identifier, Type type) {
			_identifier = identifier;
			_type = type;
		}

		protected Message(Type type) {
			_identifier = NextIdentifier++;
			_type = type;
		}

		protected Message(Message other) {
			_identifier = other._identifier;
			_type = other._type;
		}

		/// <inheritdoc />
		public virtual int CalculateSize() => 5;

		/// <inheritdoc />
		public abstract Message DeepClone();

		/// <inheritdoc/>
		public abstract bool Equals(Message other);

		public abstract void Merge(Message other);

		public virtual void ReadFrom(Stream stream) {
			if (stream == null)
				throw new ArgumentNullException(nameof(stream));
			_identifier = stream.ReadUInt32();
			_type = (Type) stream.ReadByte();
		}

		public virtual void WriteTo(Stream stream) {
			if (stream == null)
				throw new ArgumentNullException(nameof(stream));
			stream.Write(_identifier);
			stream.WriteByte((byte) _type);
		}


	}

	#if false
	/**
	 * Abstract RPC Message Implementation.
	 */
	public abstract class Message : IMessage<Message> {
		private static int NextIdentifier = 0;

		private byte identifier;
		private Type type;

		public static Message ReadMessageFrom(byte[] buffer) {
			if (!Decoder.DecodeVarint(buffer, out ulong result))
				throw new MalformedMessageException("Header incomplete. Cannot decode varint.");
			int varintSize = Encoder.ComputeVarintSize(result);
			if (buffer.Length < varintSize)
				throw new MalformedMessageException(
					"Header incomplete. Message stops right after length describer varint.");
			Message message;
			switch ((Type) buffer[varintSize + 1]) {
				case Type.Procedure | Type.Request:
					message = new ProcedureRequest {
						identifier = buffer[varintSize]
					};
					break;
				case Type.Procedure | Type.Response:
					message = new ProcedureResponse {
						identifier = buffer[varintSize]
					};
					break;
				default:
					throw new MalformedMessageException("Cannot recognize header.type field.",
						new ArgumentOutOfRangeException("type", (Type) buffer[varintSize],
							"Type does not fit requirements or values."));
			}
			message.ReadFrom(buffer, varintSize + 2);
			return message;
		}

		protected Message(byte identifier, Type type) {
			this.identifier = this.identifier;
			this.type = type;
		}

		protected Message(Type type) {
			identifier = (byte) NextIdentifier++;
			this.type = type;
		}

		public abstract int ComputeSize();

		public abstract int ReadFrom(byte[] buffer);

		public int ReadFrom(byte[] buffer, int offset) {
			byte[] copy = new byte[buffer.Length - offset];
			Array.Copy(buffer, offset, copy, 0, buffer.Length - offset);
			return ReadFrom(copy);
		}

		public byte[] ToArray() => Encoding.UTF8.GetBytes(ToString());

		public override string ToString() {
			string str = default;
			Encoder.EncodeVarint((ulong) ComputeSize(), ref str);
			str += (char) identifier;
			return str + (char) type;
		}

		public virtual string ToJsonString() =>
			$"{{ \"Header\": {{ \"Length\": {ComputeSize()},  \"Identifier\": {identifier}, \"Type\": {type} }}, \"Body\": ";

		public void WriteTo(out string buffer) => buffer = ToString();

	}

#endif

		[Flags]
	public enum Type : uint {
		Ack = 0,
		Array = 1,
		Connection = 2,
		Procedure = 3,
		Disconnection = 4,

		Request = 0x10,
		Response = 0x20
	}

}