﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace kRPC.RPC.Protocol {

	public interface IDeeplyCloneable<T> {
		T DeepClone();
	}
}
