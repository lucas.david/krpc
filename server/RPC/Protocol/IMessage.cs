﻿using kRPC.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace kRPC.RPC.Protocol {

	public interface IMessage {
		int CalculateSize();

		void ReadFrom(Stream stream);

		void WriteTo(Stream stream);
	}

	public interface IMessage<T> : IMessage, IDeeplyCloneable<T>, IEquatable<T> where T : IMessage<T> {

		void Merge(T message);
	}

}
