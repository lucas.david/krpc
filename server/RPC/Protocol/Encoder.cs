﻿using kRPC.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using kRPC.RPC.Protocol;
using BitConverter = System.BitConverter;

namespace kRPC.RPC {
	internal static class Encoder {

		public static int CalculateSize(this string self) => CalculateVarintSize((uint) self.Length) + self.Length;

		#region StreamExtensions for encoding

		public static void Write(this Stream self, bool value) => self.WriteByte(value ? (byte) 1 : (byte) 0);

		public static void Write(this Stream self, short value) {
			byte[] bytes = BitConverter.GetBytes(value);
			self.Write(bytes, 0, bytes.Length);
		}

		public static void Write(this Stream self, int value) {
			byte[] bytes = BitConverter.GetBytes(value);
			self.Write(bytes, 0, bytes.Length);
		}

		public static void Write(this Stream self, long value) {
			byte[] bytes = BitConverter.GetBytes(value);
			self.Write(bytes, 0, bytes.Length);
		}

		public static void Write(this Stream self, ushort value) {
			byte[] bytes = BitConverter.GetBytes(value);
			self.Write(bytes, 0, bytes.Length);
		}

		public static void Write(this Stream self, uint value) {
			byte[] bytes = BitConverter.GetBytes(value);
			self.Write(bytes, 0, bytes.Length);
		}

		public static void Write(this Stream self, ulong value) {
			byte[] bytes = BitConverter.GetBytes(value);
			self.Write(bytes, 0, bytes.Length);
		}

		public static void Write(this Stream self, string value) {
			byte[] bytes = Encoding.UTF8.GetBytes(value);
			self.WriteLength(bytes.Length);
			self.Write(bytes, 0, bytes.Length);
		}

		public static void Write(this Stream self, IMessage value) {
			self.WriteLength(value.CalculateSize());
			value.WriteTo(self);
		}

		/**
		 * <summary>
		 *  Writes a length on this stream.
		 *
		 *  Underlying encoding scheme for length is the LEB128, which is a
		 *  variable length integer //todo
		 * </summary>
		 * <param name="self">targeted stream</param>
		 * <param name="length">length to write</param>
		 */
		public static void WriteLength(this Stream self, int length) {
			EncodeVarint((uint) length, out byte[] buffer);
			self.Write(buffer, 0, buffer.Length);
		}

		/**
		 *
		 */
		public static void WriteTag(this Stream self, uint tag) => self.WriteByte((byte) tag);

		#endregion

		#region LEB128

		/* LEB128 variable-integer algorithms */

		public static int CalculateVarintSize(ushort value) => CalculateVarintSize((ulong) value);

		public static int CalculateVarintSize(uint value) => CalculateVarintSize((ulong) value);

		public static int CalculateVarintSize(ulong value) {
			int size = default;
			do {
				value >>= 7;
				size++;
			} while (value != 0);

			return size;
		}

		public static void EncodeVarint(ushort value, ref string buffer) => EncodeVarint((ulong) value, ref buffer);

		public static void EncodeVarint(uint value, ref string buffer) => EncodeVarint((ulong) value, ref buffer);

		public static void EncodeVarint(ulong value, ref string buffer) {
			do {
				buffer += (char) (value & 0x7F);
				value >>= 7;
			} while (value > 0x7F);

			buffer += (char) (value | 0x80);
		}

		public static void EncodeVarint(ushort value, out byte[] buffer) => EncodeVarint((ulong) value, out buffer);

		public static void EncodeVarint(uint value, out byte[] buffer) => EncodeVarint((ulong) value, out buffer);

		public static void EncodeVarint(ulong value, out byte[] buffer) {
			string strBuffer = default;
			EncodeVarint(value, ref strBuffer);
			buffer = Encoding.UTF8.GetBytes(strBuffer);
		}

		/* Signed LEB128 variable-signed interger algorithms */

		public static int ComputeSignedVarintSize(long value) {
			return 0;
		}

		public static void EncodeSignedVarint(long value, ref string buffer) { }

		public static void EncodeSignedVarint(long value, out byte[] buffer) {
			buffer = default;
		}

		#endregion

	}
}