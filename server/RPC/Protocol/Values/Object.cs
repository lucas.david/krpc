﻿using System;
using System.Collections.Generic;
using System.IO;
using kRPC.RPC.Protocol.Collections;

namespace kRPC.RPC.Protocol.Values {

	/**
	 * <summary>
	 *  <c>Object</c> represents a mapped data value, consisting of fields
	 *  which map to dynamically typed values.
	 *  <para>
	 *   The JSON representation for <c>Object</c> is JSON object.
	 *  </para>
	 * </summary>
	 */
	public sealed class Object : IMessage<Object> {
		private readonly Map<string, Value> _fields = new Map<string, Value>();

		public Map<string, Value> Fields => _fields;

		#region Constructors

		public Object() { }

		public Object(Object other) => _fields = other._fields.DeepClone();

		public Object(string key, Value value) => _fields.Add(key, value);

		#endregion

		/** <inheritdoc/> */
		public override bool Equals(object other) => Equals(other as Object);

		/** <inheritdoc/> */
		public override int GetHashCode() => 1 ^ _fields.GetHashCode();

		/** <inheritdoc/> */
		public override string ToString() => new JSON.Stringifier().Stringify(this, "\t");

		public static implicit operator Value(Object value) => new Value(value);

		#region IMessage<T> public methods

		public int CalculateSize() {
			int size = 0;
			foreach (KeyValuePair<string, Value> entry in _fields)
				size += entry.Key.CalculateSize() + entry.Value.CalculateSize();
			return size;
		}

		public Object DeepClone() => new Object(this);

		public bool Equals(Object other) {
			if (other == null)
				return false;
			return ReferenceEquals(this, other) || _fields.Equals(other._fields);
		}

		/**
		 * <summary>
		 *  Adds fields from another object in <paramref cref="this"/> object,
		 *  if <paramref cref="this"/> object does not already contains the
		 *  field.
		 * </summary>
		 * <param name="other>object to merge<param/>
		 * <exceptions cref="ArgumentNullException">
		 *  <paramref name="other"/> is <c>null</c>.
		 * </exceptions>
		 */
		public void Merge(Object other) {
			if (other == null)
				throw new ArgumentNullException(nameof(other));
			_fields.Add(other._fields);
		}

		public void ReadFrom(Stream stream) {
			// TODO
		}

		public void WriteTo(Stream stream) {
			// TODO
		}

		#endregion

	}
}