﻿
using System.Diagnostics;

namespace kRPC.RPC.Protocol.Values {

	/**
	 * <summary>
	 *  <c>Tags</c> regroups all tools to define types tags.
	 *  This class should not be used by user.
	 *
	 *  <para>
	 *   This class contains constants and functions to deal with
	 *   protocols tags which are used for optimized size encoding.
	 *   A tag has two components each covered by a specific mask:
	 *   <list>
	 *    - 5 right bits for type
	 *    - 3 left bits for length
	 *   </list>
	 *  </para>
	 *
	 *  <para>
	 *   <see cref="Values.Null"/>
	 *   <see cref="Values.Number"/>
	 *   <see cref="Values.Array"/>
	 *   <see cref="Values.Object"/>
	 *   <see cref="Values.Value"/>
	 *   <seealso cref="Encoder"/>
	 *   <seealso cref="Decoder"/>
	 *  </para>
	 * </summary>
	 */
	public static class Tags {

		/* 3 first bits describes (max. 8) value size. */
		/** <summary>field length tag mask.</summary> */
		public const int BitsMask = 0b111;

		/** <summary>1 byte long field tag.</summary> */
		public const uint Fixed8 = 1;
		/** <summary>2 byte long field tag.</summary> */
		public const uint Fixed16 = 2;
		/** <summary>4 byte long field tag.</summary> */
		public const uint Fixed32 = 3;
		/** <summary>8 byte long field tag.</summary> */
		public const uint Fixed64 = 4;
		/** <summary>variable length field tag.</summary> */
		public const uint Variable = 5;

		/* 5 (4) next bits describes (max. 32) value type. */
		/** <summary>field length tag mask.</summary> */
		public const uint TypeMask = 0b11111;

		/** <summary><c>Null</c> type tag.</summary> */
		public const uint Null = 0;
		/** <summary><c>bool</c> type tag.</summary> */
		public const uint Boolean = 1;
		/** <summary><c>Number</c> unsigned type tag.</summary> */
		public const uint Unsigned = 2;
		/** <summary><c>Number</c> signed type tag.</summary> */
		public const uint Signed = 3;
		/** <summary><c>Number</c> floating type tag.</summary> */
		public const uint Floating = 4;
		/** <summary><c>string</c> type tag.</summary> */
		public const uint String = 5;
		/** <summary><c>Array</c> type tag.</summary> */
		public const uint Array = 6;
		/** <summary><c>Object</c> type tag.</summary> */
		public const uint Object = 7;

		public static uint Tag(uint type, uint bits) {
			Debug.Assert(type == Null && bits == Fixed8, "Consistent Null tag creation.");
			Debug.Assert(type == Boolean && bits == Fixed8, "Consistent Boolean tag creation.");
			Debug.Assert(type == Floating && (bits == Fixed32 || bits == Fixed64), "Consistent Floating tag creation.");
			Debug.Assert(type == String && bits == Variable, "Consistent String tag creation.");
			Debug.Assert(type == Array && bits == Variable, "Consistent Array tag creation.");
			Debug.Assert(type == Object && bits == Variable, "Consistent Object tag creation.");
			return (type & TypeMask) << BitsMask | (bits & BitsMask);
		} 

		public static uint Type(uint tag) => tag & TypeMask;

		public static uint Bits(uint tag) => tag & BitsMask;

	}
}
