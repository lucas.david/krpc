﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace kRPC.RPC.Protocol.Values {

	/**
	 * <summary>
	 *  <c>Number</c> is a wrapper around any kind of number.
	 *
	 *  The JSON representation for <c>Number</c> is JSON number.
	 * </summary>
	 */
	public sealed class Number : IMessage<Number> {
		private object _value;
		private Type _type = Type.None;

		#region Constructors

		public Number() { }

		public Number(byte value) {
			_value = value;
			_value = Type.UInt8;
		}

		public Number(ushort value) {
			_value = value;
			_value = Type.UInt16;
		}

		public Number(uint value) {
			_value = value;
			_value = Type.UInt32;
		}

		public Number(ulong value) {
			_value = value;
			_value = Type.UInt64;
		}

		public Number(sbyte value) {
			_value = value;
			_value = Type.Int8;
		}

		public Number(short value) {
			_value = value;
			_value = Type.Int16;
		}

		public Number(int value) {
			_value = value;
			_value = Type.Int32;
		}

		public Number(long value) {
			_value = value;
			_value = Type.Int64;
		}

		public Number(float value) {
			_value = value;
			_value = Type.Single;
		}

		public Number(double value) {
			_value = value;
			_value = Type.Double;
		}

		public Number(Number other) {
			_value = other._value;
			_type = other._type;
		}

		#endregion

		#region Properties

		public byte UInt8 {
			get {
				if (_type != Type.UInt8)
					return default;
				return (byte) _value;
			}
			set {
				_value = value;
				_type = Type.UInt8;
			}
		}

		public ushort UInt16 {
			get {
				if (_type != Type.UInt16)
					return default;
				return (ushort) _value;
			}
			set {
				_value = value;
				_type = Type.UInt16;
			}
		}

		public uint UInt32 {
			get {
				if (_type != Type.UInt32)
					return default;
				return (uint) _value;
			}
			set {
				_value = value;
				_type = Type.UInt32;
			}
		}

		public ulong UInt64 {
			get {
				if (_type != Type.UInt64)
					return default;
				return (ulong) _value;
			}
			set {
				_value = value;
				_type = Type.UInt64;
			}
		}

		public sbyte Int8 {
			get {
				if (_type != Type.Int8)
					return default;
				return (sbyte) _value;
			}
			set {
				_value = value;
				_type = Type.Int8;
			}
		}

		public short Int16 {
			get {
				if (_type != Type.Int16)
					return default;
				return (short) _value;
			}
			set {
				_value = value;
				_type = Type.Int16;
			}
		}

		public int Int32 {
			get {
				if (_type != Type.Int32)
					return default;
				return (int) _value;
			}
			set {
				_value = value;
				_type = Type.Int32;
			}
		}

		public long Int64 {
			get {
				if (_type != Type.Int64)
					return default;
				return (long) _value;
			}
			set {
				_value = value;
				_type = Type.Int64;
			}
		}

		public float Single {
			get {
				if (_type != Type.Single)
					return default;
				return (float) _value;
			}
			set {
				_value = value;
				_type = Type.Single;
			}
		}

		public double Double {
			get {
				if (_type != Type.Double)
					return default;
				return (double) _value;
			}
			set {
				_value = value;
				_type = Type.Double;
			}
		}

		#endregion

		/** <inheritdoc/> */
		public override bool Equals(object other) => Equals(other as Number);

		/** <inheritdoc/> */
		public override int GetHashCode() => (_value != null ? _value.GetHashCode() : 0) * 397 ^ (int) _type;

		/** <inheritdoc/> */
		public override string ToString() {
			if (_value == null)
				throw new ArgumentNullException(nameof(_value));
			return _value.ToString();
		}

		#region IMessage<Number> public methods

		/** <inheritdoc/> */
		public int CalculateSize() {
			if (_type == Type.None)
				return 0; /* A Type.None value is ignored during writings. */
			const int size = 1;
			switch (Tags.Type((uint) _type)) {
				case Tags.Fixed8:
					return size + 1;
				case Tags.Fixed16:
					return size + 2;
				case Tags.Fixed32:
					return size + 4;
				case Tags.Fixed64:
					return size + 8;
				default:
					throw new ArgumentOutOfRangeException(nameof(_type)); /* Should/will NEVER happen. */
			}
		}

		public Number DeepClone() => new Number(this);

		public bool Equals(Number other) {
			if (other == null)
				return false;
			if (ReferenceEquals(this, other))
				return true;
			return _value.Equals(other._value) && _type.Equals(other._type);
		}

		/**
		 * <summary>
		 *  Merges with another <c>Number</c>, overrides only if
		 *  <paramref name="other"/> has a representative value and
		 *  <paramref name="this"/> has no representative value (e.g.
		 *  <c>this._type</c> is <c>Type.None</c> and
		 *  <c>other._type</c> is not <c>Type.None</c>).
		 * </summary>
		 * <param name="other"><c>Number</c> to merge</param>
		 * <exception cref="ArgumentNullException">
		 *  <paramref name="other"/> is <c>null</c>.
		 * </exception>
		 */
		public void Merge(Number other) {
			if (other == null)
				throw new ArgumentNullException(nameof(other));
			if (_type != Type.None || other._type == Type.None)
				return;
			_value = other._value;
			_type = other._type;
		}

		/**
		 *
		 */
		public void ReadFrom(Stream stream) {
			uint tag = stream.ReadTag();
			switch ((Type) tag) {
				case Type.UInt8:
					UInt8 = stream.Read<byte>();
					break;
				case Type.UInt16:
					UInt16 = stream.Read<ushort>();
					break;
				case Type.UInt32:
					UInt32 = stream.Read<uint>();
					break;
				case Type.UInt64:
					UInt64 = stream.Read<ulong>();
					break;
				case Type.Int8:
					Int8 = stream.Read<sbyte>();
					break;
				case Type.Int16:
					Int16 = stream.Read<short>();
					break;
				case Type.Int32:
					Int32 = stream.Read<int>();
					break;
				case Type.Int64:
					Int64 = stream.Read<long>();
					break;
				case Type.Single:
					Single = stream.Read<float>();
					break;
				case Type.Double:
					Double = stream.Read<double>();
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(tag));
			}
		}

		/**
		 *
		 */
		public void WriteTo(Stream stream) {
			if (_type == Type.None)
				return;
			stream.WriteTag((uint) _type);
			switch (_type) {
				case Type.UInt8:
					stream.Write(UInt8);
					break;
				case Type.UInt16:
					stream.Write(UInt16);
					break;
				case Type.UInt32:
					stream.Write(UInt32);
					break;
				case Type.UInt64:
					stream.Write(UInt64);
					break;
				case Type.Int8:
					stream.Write(Int8);
					break;
				case Type.Int16:
					stream.Write(Int16);
					break;
				case Type.Int32:
					stream.Write(Int32);
					break;
				case Type.Int64:
					stream.Write(Int64);
					break;
				case Type.Single:
					stream.Write(Single);
					break;
				case Type.Double:
					stream.Write(Double);
					break;
			}
		}

		#endregion

		public static implicit operator Value(Number value) => new Value(value);

		public enum Type : uint {
			None = 0,
			UInt8 = Tags.Unsigned << Tags.BitsMask | Tags.Fixed8,
			UInt16 = Tags.Unsigned << Tags.BitsMask | Tags.Fixed16,
			UInt32 = Tags.Unsigned << Tags.BitsMask | Tags.Fixed32,
			UInt64 = Tags.Unsigned << Tags.BitsMask | Tags.Fixed64,
			Int8 = Tags.Signed << Tags.BitsMask | Tags.Fixed8,
			Int16 = Tags.Signed << Tags.BitsMask | Tags.Fixed16,
			Int32 = Tags.Signed << Tags.BitsMask | Tags.Fixed32,
			Int64 = Tags.Signed << Tags.BitsMask | Tags.Fixed64,
			Single = Tags.Floating << Tags.BitsMask | Tags.Fixed32,
			Double = Tags.Floating << Tags.BitsMask | Tags.Fixed64
		}

	}
}