﻿using System.Runtime.InteropServices;

namespace kRPC.RPC.Protocol.Values {

	[StructLayout(LayoutKind.Auto, Size = 1)]
	public struct Null {
		public static Null Value;

		/** <inheritdoc/> */
		public override bool Equals(object other) => true;

		/** <inheritdoc/> */
		public override int GetHashCode() => 0;

		/** <inheritdoc/> */
		public override string ToString() => "null";

		public bool Equals(Null other) => true;

		public static implicit operator Value(Null value) => new Value(value);
	}
}
