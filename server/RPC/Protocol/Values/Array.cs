﻿using System;
using System.IO;
using System.Linq;
using kRPC.RPC.Protocol.Collections;

namespace kRPC.RPC.Protocol.Values {

	/**
	 * <summary>
	 * <c>Array</c> is a wrapper around a list of values.
	 *
	 * The JSON representation for <c>Array</c> is JSON array.
	 * </summary>
	 */
	public sealed class Array : IMessage<Array> {
		private static readonly Value[] EmptyArray = new Value[0];

		private readonly ArrayList<Value> _values = new ArrayList<Value>();

		#region Constructors

		public Array() {}

		public Array(Array other) => _values = other._values.DeepClone();

		public ArrayList<Value> Values => _values;

		#endregion

		/** <inheritdoc/> */
		public override bool Equals(object other) => Equals(other as Array);

		/** <inheritdoc/> */
		public override int GetHashCode() => 1 ^ _values.GetHashCode();

		/** <inheritdoc/> */
		public override string ToString() => new JSON.Stringifier().Stringify(this, "\t");

		#region IMessage<Array> public methods

		public int CalculateSize() => _values.Sum(value => value.CalculateSize());

		public Array DeepClone() => new Array(this);

		public bool Equals(Array other) {
			if (other == null)
				return false;
			return ReferenceEquals(this, other) || _values.Equals(other._values);
		}

		public void Merge(Array message) {
			if (message == null)
				throw new ArgumentNullException(nameof(message));
			_values.Add(message._values);
		}

		public void ReadFrom(Stream stream) {
			// TODO
		}

		public void WriteTo(Stream stream) {
			// TODO
		}

		#endregion

		public static implicit operator Value(Array value) => new Value(value);

	}
}