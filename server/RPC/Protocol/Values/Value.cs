﻿using System;
using System.IO;

namespace kRPC.RPC.Protocol.Values {

	/**
	 * <summary>
	 *  <c>Value</c> represents a dynamically typed value which can be either
	 *  <c>Null</c>, a <c>Number</c>, a <c>String</c>, a <c>Boolean</c>,
	 *  an <c>Array</c>, or an <c>Object</c>. A producer of value is expected
	 *  to set one of that variants, absence of any variant indicates an error.
	 *  <para>
	 *   The JSON representation for <c>Value</c> is JSON dynamically typed
	 *   value.
	 *  </para>
	 * </summary>
	 */
	public sealed class Value : IMessage<Value> {
		private object _value;
		private Type _type = Type.None;

		#region Constructors

		public Value() { }

		public Value(Null value) => Null = value;

		public Value(bool value) => Boolean = value;

		public Value(Number value) => Number = value;

		public Value(string value) => String = value;

		public Value(Array value) => Array = value;

		public Value(Object value) => Object = value;

		public Value(Value other) {
			switch (other._type) {
				case Type.Null:
					Null = other.Null;
					break;
				case Type.Boolean:
					Boolean = other.Boolean;
					break;
				case Type.Number:
					Number = other.Number;
					break;
				case Type.String:
					String = other.String;
					break;
				case Type.Object:
					Object = other.Object.DeepClone();
					break;
				case Type.Array:
					Array = Array.DeepClone();
					break;
			}
		}

		#endregion

		#region Properties

		/**
		 * <summary>Represents the underlying type stored.</summary>
		 * <value>Gets value type field, _type</value>
		 */
		public Type UnderlyingType => _type;

		public Null Null {
			get => Null;
			set {
				_value = value;
				_type = Type.Null;
			}
		}

		public bool Boolean {
			get {
				if (_type != Type.Boolean)
					return false;
				return (bool) _value;
			}
			set {
				_value = value;
				_type = Type.Boolean;
			}
		}

		public Number Number {
			get {
				if (_type != Type.Number)
					return null;
				return (Number) _value;
			}
			set {
				_value = value;
				_type = Type.Number;
			}
		}

		public string String {
			get {
				if (_type != Type.String)
					return string.Empty;
				return (string) _value;
			}
			set {
				_value = value ?? throw new ArgumentNullException(nameof(value));
				_type = Type.String;
			}
		}

		public Array Array {
			get {
				if (_type != Type.Array)
					return null;
				return (Array) _value;
			}
			set {
				_value = value ?? throw new ArgumentNullException(nameof(value));
				_type = Type.Array;
			}
		}

		public Object Object {
			get {
				if (_type != Type.Object)
					return null;
				return (Object)_value;
			}
			set {
				_value = value ?? throw new ArgumentNullException(nameof(value));
				_type = Type.Object;
			}
		}

		#endregion

		/** <inheritdoc/> */
		public override bool Equals(object other) => Equals(other as Value);

		/** <inheritdoc/> */
		public override int GetHashCode() {
			int hash = 1;
			switch (_type) {
				case Type.Null:
					hash ^= Null.GetHashCode();
					break;
				case Type.Boolean:
					hash ^= Boolean.GetHashCode();
					break;
				case Type.Number:
					hash ^= Number.GetHashCode();
					break;
				case Type.String:
					hash ^= String.GetHashCode();
					break;
				case Type.Object:
					hash ^= Object.GetHashCode();
					break;
				case Type.Array:
					hash ^= Array.GetHashCode();
					break;
			}

			return hash ^ (int) _type;
		}

		/** <inheritdoc/> */
		public override string ToString() {
			if (_type == Type.None)
				throw new ArgumentException(nameof(_type));
			return new JSON.Stringifier().Stringify(this, "\t");
		}

		#region IMessage<Value> public methods

		public int CalculateSize() {
			const int size = 1;
			switch (_type) {
				case Type.None:
					return 0; /* A Type.None value is ignored during writings. */
				case Type.Null:
					return size;
				case Type.Boolean:
					return size + 1;
				case Type.Number:
					return size + Number.CalculateSize();
				case Type.String:
					return size + String.CalculateSize();
				case Type.Array:
					return size + Array.CalculateSize();
				case Type.Object:
					return size + Object.CalculateSize();
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public Value DeepClone() => new Value(this);

		public bool Equals(Value other) {
			if (other == null)
				return false;
			if (ReferenceEquals(this, other))
				return true;
			return _type == other._type && _value.Equals(other);
		}

		/**
		 * <summary>
		 *  Merges with another <c>Value</c>, overrides only if
		 *  <paramref name="other"/> has a representative value and
		 *  current has no representative value (e.g.
		 *  <c>_type</c> is <c>Type.None</c> and
		 *  <c>other._type</c> is not <c>Type.None</c>).
		 * </summary>
		 * <param name="other"><c>Value</c> to merge</param>
		 * <exception cref="ArgumentNullException">
		 *  <paramref name="other"/> is <c>null</c>.
		 * </exception>
		 */
		public void Merge(Value other) {
			if (other == null)
				throw new ArgumentNullException(nameof(other));
			if (_type != Type.None || other._type == Type.None)
				return;
			_value = other._value;
			_type = other._type;
		}

		public void ReadFrom(Stream stream) {
			// TODO
		}

		public void WriteTo(Stream stream) {
			// TODO
		}

		#endregion

		public enum Type : uint {
			None = 0,
			Null = Tags.Null << Tags.BitsMask | Tags.Fixed8,
			Boolean = Tags.Boolean << Tags.BitsMask | Tags.Fixed8,
			Number, /* Should NOT be used as a tag. Use underlying tags which precise number nature. */
			String = Tags.String << Tags.BitsMask | Tags.Variable,
			Array = Tags.Array << Tags.BitsMask | Tags.Variable,
			Object = Tags.Object << Tags.BitsMask | Tags.Variable
		}

		public static implicit operator Value(string value) => new Value(value);
	}

	#if false
	public interface IValue : IMessage<IValue> {
		object ObjectValue { get; }
	}

	public abstract class ValueBase : IValue, IMessage<ValueBase> {
		private byte[] value;

		public int ComputeSize() => throw new NotImplementedException();

		public int ReadFrom(byte[] buffer) => throw new NotImplementedException();

		public int ReadFrom(byte[] buffer, int offset) => throw new NotImplementedException();

		public byte[] ToArray() => throw new NotImplementedException();

		public string ToJsonString() => throw new NotImplementedException();

		public void WriteTo(out string buffer) => throw new NotImplementedException();

		public object ObjectValue { get; }
	}

	/* Write value */

	public struct ArrayValue<T> : IValue, IMessage<ArrayValue<T>> where T : unmanaged {
		public T[] values;

		public object ObjectValue => values;

		public int ComputeSize() => values.Length * Unsafe.SizeOf<T>();

		public int ReadFrom(byte[] buffer) => ReadFrom(buffer, 0);

		public int ReadFrom(byte[] buffer, int offset) {
			int sizeOf = Unsafe.SizeOf<T>();
			if (buffer.Length - offset < values.Length * sizeOf)
				throw new MalformedMessageException("Not enough bytes left for parameters[?].values.",
					new ArgumentOutOfRangeException(nameof(buffer)));
			for (int i = 0; i < values.Length; i++, offset += sizeOf)
				BitConverter.ToTee<T>(buffer, offset);
			return values.Length * sizeOf;
		}

		public byte[] ToArray() => Encoding.UTF8.GetBytes(ToString());

		public override string ToString() {
			string str = new string((char) (values.Length << 2 | (int) (Math.Log(Unsafe.SizeOf<T>()) / Math.Log(2))),
				1);
			return values.Aggregate(str,
				(current1, v) => BitConverter.GetBytes(v).Aggregate(current1, (current, b) => current + (char) b));
		}

		public string ToJsonString() {
			string str = $"{{ \"ArrayCount\": {values.Length}, \"TypeSize\": {Unsafe.SizeOf<T>()}, \"Value\": [";
			for (int i = 0; i < values.Length - 1; i++)
				str += $" {values[i]},";
			return str += $" {values[values.Length - 1]} ] }}";
		}

		public void WriteTo(out string buffer) => buffer = ToString();
	}

	/*
	public struct Value<T> : IValue, IMessage<Value<T>> where T : unmanaged {
		public T value;

		public object ObjectValue => value;

		public int ComputeSize() => Unsafe.SizeOf<T>();


		public int ReadFrom(byte[] buffer) => ReadFrom(buffer, 0);

		public int ReadFrom(byte[] buffer, int offset) {
			int sizeOf = Unsafe.SizeOf<T>();
			if (buffer.Length - offset < sizeOf)
				throw new MalformedMessageException("Not enough bytes left for parameters[?].value.",
					new ArgumentOutOfRangeException(nameof(buffer)));
			BitConverter.ToTee<T>(buffer, offset);
			return sizeOf;
		}

		public byte[] ToArray() => Encoding.UTF8.GetBytes(ToString());

		public override string ToString() =>
			BitConverter.GetBytes(value)
				.Aggregate(new string((char) (0b0100 | (int) (Math.Log(Unsafe.SizeOf<T>()) / Math.Log(2))), 1),
					(current, b) => current + (char) b);

		public string ToJsonString() => $"{{ \"TypeSize\": {Unsafe.SizeOf<T>()}, \"Value\": {value} }}";

		public void WriteTo(out string buffer) => buffer = ToString();

	}
	*/
	#endif
}