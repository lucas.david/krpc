﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace kRPC.RPC.Protocol.Exceptions {

	internal class MalformedMessageException : Exception {

		public MalformedMessageException() {}

		public MalformedMessageException(string message) : base(message) {}

		public MalformedMessageException(string message, Exception innerException) : base(message, innerException) {}

	}

}
