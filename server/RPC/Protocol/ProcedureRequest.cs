﻿using System;
using System.IO;
using kRPC.RPC.Protocol.Values;
using Array = kRPC.RPC.Protocol.Values.Array;
using Object = kRPC.RPC.Protocol.Values.Object;

namespace kRPC.RPC.Protocol {

	public class ProcedureRequest : Message, IMessage<ProcedureRequest> {
		public const int MinIdentifierLength = 0;
		public const int MaxIdentifierLength = 256;

		private string _identifier;
		private readonly Array _parameterList = new Array();

		public ProcedureRequest() : base(Type.Procedure | Type.Request) { }
		
		public ProcedureRequest(ProcedureRequest other) : base(other) {
			_identifier = (string) other._identifier.Clone();
			_parameterList = other._parameterList.DeepClone();
		}

		#region Public properties

		public string Identifier {
			get => _identifier;
			set {
				if (value == null)
					throw new ArgumentNullException(nameof(value));
				if (value.Length < MinIdentifierLength || MaxIdentifierLength < value.Length)
					throw new ArgumentOutOfRangeException(nameof(value), value,
						$"Identifier should be sized between {MinIdentifierLength} and {MaxIdentifierLength}.");
				_identifier = value;
			}
		}

		public Array ParameterList => _parameterList;

		#endregion

		public override string ToString() => new JSON.Stringifier().Stringify((Value) this, "\t");

		#region Message, IMessage<ProcedureRequest> public methods

		public override int CalculateSize() =>
			base.CalculateSize() + _identifier.CalculateSize() + _parameterList.CalculateSize();

		/// <inheritdoc />
		public override Message DeepClone() => ((IDeeplyCloneable<ProcedureRequest>) this).DeepClone();

		/// <inheritdoc />
		public override bool Equals(Message other) => Equals(other as ProcedureRequest);

		/// <inheritdoc />
		public bool Equals(ProcedureRequest other) {
			if (other == null)
				return false;
			if (ReferenceEquals(this, other))
				return true;
			return Equals(_identifier, other._identifier) && _parameterList.Equals(other._parameterList);
		}

		public override void Merge(Message other) => Merge(other as ProcedureRequest);

		public void Merge(ProcedureRequest other) => throw new NotSupportedException();

		public override void ReadFrom(Stream stream) {
			base.ReadFrom(stream); /* reads message identifier and message type */
			stream.ReadString();
			_parameterList.ReadFrom(stream);
			#if false
			int index = Array.FindIndex(buffer, b => b == 0);
			_identifier = Encoding.UTF8.GetString(buffer, 0, index);
			if (index == buffer.Length)
				throw new MalformedMessageException("Message lasts with body._identifier.");
			index++;
			for (; index < buffer.Length; index++) {
				IValue value;
				switch ((1 < buffer[index] >> 2 ? 0b100 : 0b0) | buffer[index] & 0b11) {
					case 0b000:
						value = new Value<byte>();
						break;
					case 0b001:
						value = new Value<short>();
						break;
					case 0b010:
						value = new Value<int>();
						break;
					case 0b011:
						value = new Value<long>();
						break;
					case 0b100:
						value = new ArrayValue<byte> { values = new byte[buffer[index] >> 2]};
						break;
					case 0b101:
						value = new ArrayValue<short> { values = new short[buffer[index] >> 2] };
						break;
					case 0b110:
						value = new ArrayValue<int> { values = new int[buffer[index] >> 2] };
						break;
					case 0b111:
						value = new ArrayValue<long> { values = new long[buffer[index] >> 2] };
						break;
					default:
						throw new ArgumentOutOfRangeException("body.parameter[?].size", buffer[index] & 0b11, "");
				}
				index += value.ReadFrom(buffer, index);
			}
			return index;
			#endif
		}

		public override void WriteTo(Stream stream) {
			base.WriteTo(stream); /* writes message identifier and message type */
			stream.Write(_identifier);
			_parameterList.WriteTo(stream);
		}

		#endregion

		ProcedureRequest IDeeplyCloneable<ProcedureRequest>.DeepClone() => new ProcedureRequest(this);

		public static explicit operator Value(ProcedureRequest procedureRequest) {
			Object value = new Object();
			value.Fields.Add("identifier",
				procedureRequest._identifier); /* implicit conversion string - wrapped -> Value */
			value.Fields.Add("parameter-list",
				procedureRequest._parameterList); /* implicit conversion Array - wrapped -> Value */
			return value; /* implicit conversion Object - wrapped -> Value */
		}

	}

}