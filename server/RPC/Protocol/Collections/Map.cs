﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace kRPC.RPC.Protocol.Collections {

	/**
	 * <summary>
	 *  <c>Map</c> is an associative container that contains key-value
	 *  pairs with unique non-null keys.
	 * </summary>
	 * <typeparam name"Key">The key type which indexes container.</typeparam>
	 * <typeparam name"Value">The value type stored in container.</typeparam>
	 */
	public sealed class Map<Key, Value> : IDictionary<Key, Value>, IDeeplyCloneable<Map<Key, Value>>,
		IEquatable<Map<Key, Value>> {

		private readonly Dictionary<Key, Value> _map = new Dictionary<Key, Value>();

		#region IDictionary<Key, Value> public properties and indexer

		public int Count => _map.Count;

		public bool IsReadOnly => false;

		public ICollection<Key> Keys => _map.Keys;

		public ICollection<Value> Values => _map.Values;

		/**
		 * <summary>
		 *  <c>Item[Key]<c/> property represents value stored at a given
		 *  <c>Value</c> key.
		 * </summary>
		 * <param name="key">the key of the element to find</param>
		 * <value>
		 *  The value that is mapped to a key equivalent to
		 *  <paramref name="key"/>, performing an insertion if such key
		 *  does not already exist. 
		 * </value>
		 * <exception cref="ArgumentNullException">
		 *  <paramref name="value"/> or <paramref name="key"/> is
		 *  <c>null</c>.
		 * </exception>
		 * <exception cref="KeyNotFoundException">
		 *  <paramref name="key"/> does not exist in container.
		 * </exception>
		 */
		public Value this[Key key] {
			get {
				if (key == null)
					throw new ArgumentNullException(nameof(key));
				if (_map.TryGetValue(key, out Value value))
					return value;
				throw new KeyNotFoundException();
			}
			set {
				if (key == null)
					throw new ArgumentNullException(nameof(key));
				if (value == null)
					throw new ArgumentNullException(nameof(value));
				_map[key] = value;
			}
		}

		#endregion

		#region IDeeplyCloneable<ArrayList<T>>, IEquatable<ArrayList<T>> public methods

		public Map<Key, Value> DeepClone() {
			Map<Key, Value> deepClone = new Map<Key, Value>();
			if (typeof(IDeeplyCloneable<Value>).IsAssignableFrom(typeof(Value)))
				foreach (KeyValuePair<Key, Value> entry in _map)
					deepClone._map[entry.Key] = ((IDeeplyCloneable<Value>) entry.Value).DeepClone();
			else
				deepClone.Add(this);
			return deepClone;
		}

		public bool Equals(Map<Key, Value> other) {
			if (other == null)
				return false;
			if (ReferenceEquals(this, other))
				return true;
			foreach (KeyValuePair<Key, Value> entry in this)
				if (!other.TryGetValue(entry.Key, out Value value) || !Equals(value, entry.Value))
					return false;
			return true;
		}

		#endregion

		public override bool Equals(object other) => Equals(other as Map<Key, Value>);

		public override int GetHashCode() {
			int hash = 1;
			foreach (KeyValuePair<Key, Value> entry in this)
				hash ^= entry.Key.GetHashCode() ^ entry.Value.GetHashCode();
			return hash;
		}

		#region IDictionary<Key, Value> public methods

		/**
		 * <summary>
		 *  Adds element into the container, if the
		 *  container does not already contain an element with an
		 *  equivalent key.
		 * </summary>
		 * <param name="key">target where the new element will be added</param>
		 * <param name="value">element value to insert</param>
		 * <exception cref="ArgumentException">
		 *  <paramref name="key"/> already exists in container.
		 * </exception>
		 * <exception cref="ArgumentNullException">
		 *  <paramref name="key"/> or <paramref name="value"/> is <c>null</c>.
		 *  <see cref="Item[Key]"/>
		 * </exception>
		 */
		public void Add(Key key, Value value) {
			if (ContainsKey(key))
				throw new ArgumentException("Key already exists in map.", nameof(key));
			this[key] = value;
		}

		/**
		 * <summary>
		 *  Adds elements into the container, if the
		 *  container does not already contain elements with an
		 *  equivalent key.
		 * </summary>
		 * <param name="key">target where the new element will be added</param>
		 * <param name="value">element value to insert</param>
		 * <exception cref="ArgumentException">
		 *  <paramref name="key"/> already exists in container.
		 * </exception>
		 * <exception cref="ArgumentNullException">
		 *  <paramref name="key"/> or <paramref name="value"/> is <c>null</c>.
		 *  <see cref="Item[Key]"/>
		 * </exception>
		 * <seealso cref="Add(Key, Value)"/>
		 */
		public void Add(IEnumerable<KeyValuePair<Key, Value>> entries) {
			if (entries == null)
				throw new ArgumentNullException(nameof(entries));
			foreach (KeyValuePair<Key, Value> entry in entries)
				this[entry.Key] = entry.Value;
		}

		public void Clear() => _map.Clear();

		public bool ContainsKey(Key key) {
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return _map.ContainsKey(key);
		}

		public IEnumerator<KeyValuePair<Key, Value>> GetEnumerator() => _map.GetEnumerator();

		public bool Remove(Key key) {
			if (key == null)
				throw new ArgumentNullException(nameof(key));
			return _map.Remove(key);
		}

		public bool TryGetValue(Key key, out Value value) {
			value = default;
			if (!ContainsKey(key))
				return false;
			value = this[key];
			return true;
		}

		#endregion

		#region IDictionary<Key, Value> methods

		void ICollection<KeyValuePair<Key, Value>>.Add(KeyValuePair<Key, Value> entry) => Add(entry.Key, entry.Value);


		bool ICollection<KeyValuePair<Key, Value>>.Contains(KeyValuePair<Key, Value> item) =>
			TryGetValue(item.Key, out Value value) && Equals(item.Value, value);

		void ICollection<KeyValuePair<Key, Value>>.CopyTo(KeyValuePair<Key, Value>[] array, int index) =>
			((ICollection<KeyValuePair<Key, Value>>) _map).CopyTo(array, index);

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		bool ICollection<KeyValuePair<Key, Value>>.Remove(KeyValuePair<Key, Value> item) =>
			((ICollection<KeyValuePair<Key, Value>>) _map).Remove(item);

		#endregion

	}
}