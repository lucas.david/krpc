﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace kRPC.RPC.Protocol.Collections {

	/**
	 *
	 */
	public sealed class ArrayList<T> : IList<T>, IList, IDeeplyCloneable<ArrayList<T>>, IEquatable<ArrayList<T>>
		where T : IEquatable<T> {
		private static readonly T[] EmptyArray = new T[0];

		private const int MinArraySize = 8;

		private T[] _array = EmptyArray;
		private int _count;

		#region IList<T>, IList public properties and indexer

		public int Count => _count;

		public bool IsReadOnly => false;

		public T this[int index] {
			get {
				if (index < 0 || _count < index)
					throw new ArgumentOutOfRangeException(nameof(index));
				return _array[index];
			}
			set {
				if (index < 0 || _count < index)
					throw new ArgumentOutOfRangeException(nameof(index));
				if (value == null)
					throw new ArgumentNullException(nameof(value));
				_array[index] = value;
			}
		}

		#endregion

		private void Reserve(int size) {
			if (size < _array.Length)
				return;
			size = Math.Max(size, MinArraySize);
			Array.Resize(ref _array, Math.Max(_array.Length * 2, size));
		}

		#region IDeeplyCloneable<ArrayList<T>>, IEquatable<ArrayList<T>> public methods

		public ArrayList<T> DeepClone() {
			ArrayList<T> deepClone = new ArrayList<T>();
			if (_array != EmptyArray) {
				deepClone._array = (T[])_array.Clone();
				if (_array is IDeeplyCloneable<T>[] deeplyCloneables)
					for (int index = 0; index < _count; ++index)
						deepClone._array[index] = deeplyCloneables[index].DeepClone();
			}

			deepClone._count = _count;
			return deepClone;
		}

		public bool Equals(ArrayList<T> other) {
			if (other == null)
				return false;
			if (ReferenceEquals(this, other))
				return true;
			if (_count != other._count)
				return false;
			for (int index = 0; index < _count; ++index)
				if (!_array[index].Equals(other._array[index]))
					return false;
			return true;
		}

		#endregion

		public override bool Equals(object obj) => Equals(obj as ArrayList<T>);

		public override int GetHashCode() {
			int hash = 0;
			for (int index = 0; index < _count; ++index)
				hash = hash * 31 + _array[index].GetHashCode();
			return hash;
		}

		#region IList<T>, IList public methods

		public void Add(T item) {
			if (item == null)
				throw new ArgumentNullException(nameof(item));

			_array[_count++] = item;
		}

		public void Add(IEnumerable<T> values) {
			if (values == null)
				throw new ArgumentNullException(nameof(values));
			if (values is ArrayList<T> arrayList) {
				Reserve(_count + arrayList._count);
				Array.Copy(arrayList._array, 0, _array, _count, arrayList._count);
				_count += arrayList._count;
			} else {
				if (values is ICollection collection) {
					int count = collection.Count;
					if (default(T) == null) {
						foreach (object obj in collection)
							if (obj == null)
								throw new ArgumentNullException(nameof(values),
									"Enumerable sequence contained null item.");
					}

					Reserve(_count + count);
					collection.CopyTo(_array, _count);
					_count += count;
				} else
					foreach (T obj in values)
						Add(obj);
			}
		}

		public void Clear() {
			_array = EmptyArray;
			_count = 0;
		}

		public bool Contains(T item) => IndexOf(item) != -1;

		public void CopyTo(T[] array, int index) => _array.CopyTo(array, index);

		public IEnumerator<T> GetEnumerator() {
			for (int index = 0; index < _count; ++index)
				yield return _array[index];
		}

		public int IndexOf(T item) {
			if (item == null)
				throw new ArgumentNullException(nameof(item));
			for (int index = 0; index < _count; ++index)
				if (_array[index].Equals(item))
					return index;
			return -1;
		}

		public void Insert(int index, T item) {
			if (item == null)
				throw new ArgumentNullException(nameof(item));
			if (index < 0 || _count < index)
				throw new ArgumentOutOfRangeException(nameof(index));
			Reserve(_count + 1);
			Array.Copy(_array, index, _array, index + 1, _count - index);
			_array[_count++] = item;
		}

		public bool Remove(T item) {
			int index = IndexOf(item);
			if (index == -1)
				return false;
			Array.Copy(_array, index + 1, _array, index, _count - index - 1);
			_array[--_count] = default;
			return true;
		}

		public void RemoveAt(int index) {
			if (index < 0 || _count < index)
				throw new ArgumentOutOfRangeException(nameof(index));
			Array.Copy(_array, index + 1, _array, default, _count - index - 1);
			_array[--_count] = default;
		}

		#endregion

		#region IList<T>, IList properties, indexer and methods

		bool IList.IsFixedSize => false;

		bool ICollection.IsSynchronized => false;

		object ICollection.SyncRoot => this;

		object IList.this[int index] {
			get => this[index];
			set => this[index] = (T) value;
		}

		int IList.Add(object value) {
			Add((T) value);
			return _count - 1;
		}

		bool IList.Contains(object value) {
			if (value is T tee)
				return Contains(tee);
			return false;
		}

		void ICollection.CopyTo(Array array, int index) => _array.CopyTo(array, index);

		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		int IList.IndexOf(object value) {
			if (!(value is T tee))
				return -1;
			return IndexOf(tee);
		}

		void IList.Insert(int index, object value) => Insert(index, (T) value);

		void IList.Remove(object value) {
			if (!(value is T tee))
				return;
			Remove(tee);
		}

		#endregion

	}
}