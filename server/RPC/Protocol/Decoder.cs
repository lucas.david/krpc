﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace kRPC.RPC.Protocol {
	public static class Decoder {

		public static bool ReadBool(this Stream self) => self.ReadByte() != 0;

		public static ushort ReadUInt16(this Stream self) {
			byte[] buffer = new byte[2];
			self.Read(buffer, 0, 2);
			return System.BitConverter.ToUInt16(buffer, 0);
		}

		public static uint ReadUInt32(this Stream self) {
			byte[] buffer = new byte[4];
			self.Read(buffer, 0, 4);
			return System.BitConverter.ToUInt32(buffer, 0);
		}

		public static ulong ReadUInt64(this Stream self) {
			byte[] buffer = new byte[8];
			self.Read(buffer, 0, 8);
			return System.BitConverter.ToUInt64(buffer, 0);
		}

		public static short ReadInt16(this Stream self) {
			byte[] buffer = new byte[2];
			self.Read(buffer, 0, 2);
			return System.BitConverter.ToInt16(buffer, 0);
		}

		public static int ReadInt32(this Stream self) {
			byte[] buffer = new byte[4];
			self.Read(buffer, 0, 4);
			return System.BitConverter.ToInt32(buffer, 0);
		}

		public static long ReadInt64(this Stream self) {
			byte[] buffer = new byte[8];
			self.Read(buffer, 0, 8);
			return System.BitConverter.ToInt64(buffer, 0);
		}

		public static float ReadSingle(this Stream self) {
			byte[] buffer = new byte[4];
			self.Read(buffer, 0, 4);
			return System.BitConverter.ToSingle(buffer, 0);
		}

		public static double ReadDouble(this Stream self) {
			byte[] buffer = new byte[8];
			self.Read(buffer, 0, 8);
			return System.BitConverter.ToDouble(buffer, 0);
		}

		public static string ReadString(this Stream self) {
			byte[] buffer = new byte[self.ReadLength()];
			self.Read(buffer, 0, buffer.Length);
			return Encoding.UTF8.GetString(buffer);
		}

		public static int ReadLength(this Stream self) {
			List<byte> buffer = new List<byte>();
			ulong result; /* for compatibility purpose using ulong */
			do
				buffer.Add((byte) self.ReadByte());
			while (!DecodeVarint(buffer.ToArray(), out result) && buffer.Count <= Max4BytesVarintLength);
			if (Max4BytesVarintLength < buffer.Count)
				throw new ArgumentException(); // TODO
			return (int) result;
		}

		public static uint ReadTag(this Stream self) => (uint) self.ReadByte();

		/* LEB128 variable-integer algorithms */

		public const int Max2BytesVarintLength = 3;
		public const int Max4BytesVarintLength = 5;
		public const int Max8BytesVarintLength = 11;

		public static bool DecodeVarint(byte[] encoded, out ushort result) {
			result = default;
			int index = 0;
			while (index < encoded.Length) {
				result <<= 7;
				result |= (byte) (encoded[index] & 0x7F);
				if (encoded[index++] < 0x80) {
					result <<= 7;
					result |= encoded[index];
					return true;
				}
			}

			return false;
		}

		public static bool DecodeVarint(byte[] encoded, out uint result) {
			result = default;
			int index = 0;
			while (index < encoded.Length) {
				result <<= 7;
				result |= (byte) (encoded[index] & 0x7F);
				if (encoded[index++] < 0x80) {
					result <<= 7;
					result |= encoded[index];
					return true;
				}
			}

			return false;
		}

		public static bool DecodeVarint(byte[] encoded, out ulong result) {
			result = default;
			int index = 0;
			while (index < encoded.Length) {
				result <<= 7;
				result |= (byte) (encoded[index] & 0x7F);
				if (encoded[index++] < 0x80) {
					result <<= 7;
					result |= encoded[index];
					return true;
				}
			}

			return false;
		}

		/* Signed LEB128 variable-signed interger algorithms */



	}
}