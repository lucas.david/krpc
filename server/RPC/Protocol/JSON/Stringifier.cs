﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kRPC.RPC.Protocol.Values;
using Array = kRPC.RPC.Protocol.Values.Array;
using Object = kRPC.RPC.Protocol.Values.Object;

namespace kRPC.RPC.Protocol.JSON {

	/**
	 *
	 */
	public sealed class Stringifier {
		private static readonly Dictionary<string, string> Replacements = new Dictionary<string, string> {
			{"\'", "\\'"},
			{"\"", "\\\""},
			{"\\", "\\\\"},
			{"\b", "\\b"},
			{"\f", "\\f"},
			{"\n", "\\n"},
			{"\r", "\\r"},
			{"\t", "\\t"},
			{"\v", "\\v"},
			{"\0", "\\0"},
			{"\u2028", "\\u2028"},
			{"\u2029", "\\u2029"}
		};

		private readonly Stack<object> stack = new Stack<object>();
		private string indent = "";
		private string gap = "";

		public string Stringify(Value value, int space) {
			if (space > 0)
				gap = "          ".Substring(0, Math.Min(10, space));
			return SerializeProperty("", new Object("", value));
		}

		public string Stringify(Value value, string space) {
			gap = space.Substring(0, 10);
			return SerializeProperty("", new Object("", value));
		}

		internal string SerializeProperty(string key, Object holder) {
			switch (holder.Fields[key].UnderlyingType) {
				case Value.Type.Null:
					return "null";
				case Value.Type.Boolean:
					return holder.Fields[key].Boolean.ToString().ToLower();
				case Value.Type.Number:
					return holder.Fields[key].Number.ToString();
				case Value.Type.String:
					return QuoteString(holder.Fields[key].String);
				case Value.Type.Array:
					return SerializeArray(holder.Fields[key].Array);
				case Value.Type.Object:
					return SerializeObject(holder.Fields[key].Object);
			}

			return null;
		}

		internal string SerializeObject(Object value) {
			if (stack.Contains(value))
				throw new ArgumentException("Converting circular structure to JSON.");

			stack.Push(value);

			string stepback = indent;
			indent += gap;

			ICollection<string> keys = value.Fields.Keys;
			Stack<string> partial = new Stack<string>();
			foreach (string key in keys) {
				string property = SerializeProperty(key, value);
				if (property != null) {
					string member = QuoteString(key) + ":";
					if (gap != "")
						member += " ";
					member += property;
					partial.Push(member);
				}
			}

			string final;
			if (partial.Count == 0)
				final = "{}";
			else {
				if (gap == "")
					final = '{' + partial.Aggregate((current, next) => current + ',' + next) + '}';
				else
					final = "{\n" + indent + partial.Aggregate((current, next) => current + ",\n" + indent + next) +
						stepback + '}';
			}

			stack.Pop();
			indent = stepback;
			return final;
		}

		internal string SerializeArray(Array value) {
			if (stack.Contains(value))
				throw new ArgumentException("Converting circular structure to JSON.");

			stack.Push(value);

			string stepback = indent;
			indent += gap;

			Stack<string> partial = new Stack<string>();
			foreach (Value val in value.Values) {
				string property = SerializeProperty("", new Object("", val);
				partial.Push(property ?? "null");
			}

			string final;
			if (partial.Count == 0)
				final = "[]";
			else {
				if (gap == "")
					final = '[' + partial.Aggregate((current, next) => current + ',' + next) + ']';
				else
					final = "[\n" + indent + partial.Aggregate((current, next) => current + ",\n" + indent + next) +
						'\n' + stepback + ']';
			}

			stack.Pop();
			indent = stepback;
			return final;
		}

		private static string QuoteString(string value) {
			Dictionary<string, double> Quotes = new Dictionary<string, double> {
				{"'", 0.1},
				{"\"", 0.2}
			};

			string product = "";

			foreach (char ch in value) {
				string c = "" + ch;
				if (c == "'" || c == "\"") {
					++Quotes[c];
					product += c;
					continue;
				}

				if (Replacements.ContainsKey(c)) {
					product += Replacements[c];
					continue;
				}

				if (c[0] < ' ') {
					string hexString = $"{c:H}";
					product += "\\x" + ("00" + hexString).Substring(hexString.Length);
					continue;
				}

				product += c;
			}

			string quoteChar = Quotes.Keys.Aggregate((a, b) => Quotes[a] < Quotes[b] ? a : b);
			product = product.Replace(quoteChar, Replacements[quoteChar]);
			return quoteChar + product + quoteChar;
		}

	}

}