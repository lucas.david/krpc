﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace kRPC.RPC.Protocol {
	internal static class Unsafe {

		public static int SizeOf<T>() where T : unmanaged {
			unsafe {
				return sizeof(T);
			}
		}

		public static TTo As<TFrom, TTo>(TFrom value) where TFrom : unmanaged where TTo : unmanaged {
			unsafe {
				return *(TTo*) &value;
			}
		}

	}
}