﻿using Google.Protobuf;
using kRPC.Network;
using System;

namespace kRPC.RPC {
	public sealed class Client : IClient, IDisposable {
		public enum AccessMode {
			Undetermined,
			ReadOnly,
			WriteOnly,
			Both
		} // TODO ajouter un champs et rajouter le support de l'accès.

		private IClient InnerClient { get; }

		public bool Connected => InnerClient.Connected; // TODO complexifier.

		public string Name { get; }

		public string RemoteAddress => InnerClient.RemoteAddress;

		public IConnection Connection => throw new NotImplementedException();

		public Client(IClient innerClient) : base() => InnerClient = innerClient;

		public void Close() => InnerClient.Close();

		public void Dispose() => InnerClient.Dispose();

		public byte[] Receive() => InnerClient.Receive();

		public int Send(byte[] buffer) => InnerClient.Send(buffer);
	}
}
