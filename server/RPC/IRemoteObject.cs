﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace kRPC.RPC {
	public interface IRemoteObject {}

	public interface IRemoteObject<T> : IRemoteObject where T : class {}
}
