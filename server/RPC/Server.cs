﻿using kRPC.Network;
using System;
using System.Collections.Generic;
using kRPC.RPC.Protocol;
using UnityEngine;

namespace kRPC.RPC {
	public class Server : MonoBehaviour, IServer {

		private readonly RemoteObjectStore _store;
		private readonly IServer _networkServer;
		private Queue<IConnection> _connectionsToHandle;

		public bool IsOpen => _networkServer.IsOpen;
		public string LocalAddress => _networkServer.LocalAddress;

		public event EventHandler Opened;
		public event EventHandler Closed;

		#region Constructors

		public Server(IServer networkServer) {
			_store = new RemoteObjectStore();
			_networkServer = networkServer ?? throw new ArgumentNullException(nameof(networkServer));
			_connectionsToHandle = new Queue<IConnection>();
		}

		public Server(string address, int port) {
			_store = new RemoteObjectStore();
			_networkServer = new Network.TCP.Server(address, port);
			_connectionsToHandle = new Queue<IConnection>();
			_networkServer.Opened += Opened;
			_networkServer.Closed += Closed;
		}

		#endregion

		#if SERIALIO_SUPPORT
		public Server(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits) {
			_store = new Store<IRemoteObject>();
			_networkServer = new Network.Serial.Server(address, port);
		}
		#endif

		#region Private class methods

		private void Handle(byte[] messageToHandle) {
			Message.ReadMessageFrom(messageToHandle);
		}

		private void Poll() {
			byte[] messageToHandle = null;
			lock (_connectionsToHandle)
				if (_connectionsToHandle.Count != 0) {
					lock (_connectionsToHandle.Peek().MessagesToHandle)
						if (_connectionsToHandle.Peek().MessagesToHandle.Count != 0)
							messageToHandle = _connectionsToHandle.Dequeue().MessagesToHandle.Dequeue();
				}
			if (messageToHandle != null)
				Handle(messageToHandle);
		}

		#endregion
		 /*
		private void OnClientRequestHandle(object sender, Network.Events.ClientRequestingEventArgs eventArgs) {
			UnityEngine.Debug.LogFormat("[kRPC]: Received: {0}", BitConverter.ToString(eventArgs.Request));
			var response = new Protocol.ProcedureResponse();
			try {
				var request = Protocol.Request.Parser.ParseFrom(eventArgs.Request);
				response.Identifier = request.Identifier;
				Type type = Type.GetType(request.Procedure.Type);
				if (type == null) {
					response.Error.Type = "Wrong type argument.";
					response.Error.Message = "Unrecognized type '" + request.Procedure.Type + "'.";
					eventArgs.Client.Send(response.ToByteArray());
					return;
				}

				UnityEngine.Debug.LogFormat("Request class name {0}", type.FullName);
				MethodInfo methodInfo = type.GetMethod(request.Procedure.Name);
				if (methodInfo == null) {
					response.Error.Type = "Wrong method name argument.";
					response.Error.Message = "Unrecognized method '" + request.Procedure.Name + "'.";
					eventArgs.Client.Send(response.ToByteArray());
					return;
				}

				UnityEngine.Debug.LogFormat("Request method name {0}", methodInfo.Name);
				object[] args = request.Procedure.Params.Count == 0 ? null : new object[request.Procedure.Params.Count];
				for (int i = 0; i < request.Procedure.Params.Count; i++)
					args[i] = Encoder.Decode(_store, request.Procedure.Params[i]);
				var handler = new RemoteMethodHandler(methodInfo);
				if (handler == null)
					UnityEngine.Debug.Log("MethodHandler is null.");
				// var result = handler.Invoke(args);
				var result = methodInfo.Invoke(null, null);
				// response.Result = Encoding.Encode(Store, result);
				eventArgs.Client.Send(Encoding.ASCII.GetBytes(result.ToString()));
			} catch (ArgumentOutOfRangeException) {
				throw;
			} catch (Exception e) {
				UnityEngine.Debug.LogFormat("{0}: {1}.", e.GetType(), e.Message);
				UnityEngine.Debug.Log(e.StackTrace);
				response.Error.Type = e.GetType().ToString();
				response.Error.Message = e.Message;
				response.Error.StackTrace = e.StackTrace;
			}
		}
		*/

		#region Public class methods

		public void Close() {
			if (!IsOpen) {
				Debug.Log("[kRPC]: Server is not opened, cannot Close().");
				return;
			}

			_networkServer.Close();
		}

		public void FixedUpdate() => Poll();

		public void Open() {
			if (IsOpen) {
				Debug.Log("[kRPC]: Server is already opened, ignoring Open() request.");
				return;
			}

			_networkServer.Open();
		}

		#endregion

		#region Disposable pattern

		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected void Dispose(bool disposing) {
			if (disposing && IsOpen) {
				_networkServer?.Dispose();
			}
		}

		#endregion

	}
}