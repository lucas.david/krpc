﻿using kRPC.Utils;

namespace kRPC.RPC {
	public interface IStore<T, S> {
		S this[T obj] { get; }

		S Add(T obj);

		void Clear();

		void Remove(T obj);
	}
}
