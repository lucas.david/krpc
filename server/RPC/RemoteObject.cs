﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace kRPC.RPC {
	public struct RemoteObject : IRemoteObject {
		public static RemoteObject Empty = new RemoteObject {index = 0, value = null};

		public ulong index;
		public object value;

	}
}