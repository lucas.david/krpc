﻿using System;
using System.Collections;
using System.Reflection;

namespace kRPC.RPC {
	class RemoteMethodHandler : IRemoteProcedureHandler {

		private readonly MethodBase _method;

		public RemoteMethodHandler(string identifier) {
			if (string.IsNullOrEmpty(identifier))
				throw new ArgumentException("string is null or empty", nameof(identifier));
			/* Check the last '.' character which is splitting type name and method name. */
			int separator = identifier.LastIndexOf('.');
			Type type = Type.GetType(identifier.Substring(0, separator)) ??
				throw new ArgumentException("string is probably malformed, type cannot be recognized.",
					nameof(identifier));
			_method = type.GetMethod(identifier.Substring(separator + 1), BindingFlags.Public) ??
				throw new ArgumentException("string is probably malformed, method cannot be recognized.",
					nameof(identifier));
		}

		public RemoteMethodHandler(MethodInfo methodInfo) {
			if (!methodInfo.IsPublic)
				throw new TargetException();
			_method = methodInfo ?? throw new ArgumentNullException(nameof(methodInfo));
			UnityEngine.Debug.Log("Instanced MethodHandler.");
		}

		public object Invoke(object[] parameters) {
			UnityEngine.Debug.Log("Entering Invoke.");
			ArrayList args = null;
			if (parameters != null) {
				args = new ArrayList();
				for (int i = _method.IsStatic ? 0 : 1; i < parameters.Length; i++) {
					if (parameters[i].GetType() != _method.GetParameters()[i - 1].GetType()) {
						if (!_method.GetParameters()[i - 1].IsOptional)
							throw new ArgumentException((_method.IsStatic ? i + 1 : i) + "th argument is not optional and is missing or does not correspond.");
						args.Add(_method.GetParameters()[i - 1].DefaultValue);
					} else
						args.Add(parameters[i]);
				}
			}
			UnityEngine.Debug.LogFormat("Before Invoke. {0}", _method == null);
			return _method.Invoke(_method.IsStatic ? parameters[0] : null, args?.ToArray());
		}
	}
}
