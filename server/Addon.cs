﻿using System;
using kRPC.Commands;
using KSP.UI.Screens.DebugToolbar;
using UnityEngine;

namespace kRPC {

	[KSPAddon(KSPAddon.Startup.Instantly, true)]
	public sealed class Addon : MonoBehaviour, IDisposable {

		private static Addon _instance;
		public static Addon Instance {
			get {
				return _instance;
			}
		}

		private IServer _server;

		public Addon() {
			_instance = this;
		}

		public IServer Server {
			get {
				return _server;
			}
			set {
				_server = value ?? throw new ArgumentNullException(nameof(value));
			}
		}

		public void Awake() {
			DontDestroyOnLoad(this);
		}

		public void Start() {
			DebugScreenConsole.AddConsoleCommand("krpc", Command.OnCommand, "Take a look at the doc for more details.");
		}

		public void OnApplicationQuit() => Dispose();

		#region Disposable pattern

		~Addon() {
			Dispose(false);
		}

		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing) {
			if (disposing && _server != null && _server.IsOpen)
				_server.Dispose();
		}

		#endregion

	}
}