﻿using System;
using kRPC.Services.Attributes;
using kRPC.Services.Exceptions;

namespace kRPC.Services {
	public static class SpaceProgram {
		public static GameScenes GameScene() => HighLogic.LoadedScene;
		public static Game.Modes GameMode() => HighLogic.CurrentGame?.Mode ?? throw new InvalidGameSceneException();

		[kRPC("active_vessel_velocity", GameScenes.FLIGHT)]
		private static Vector3d PActiveVesselVelocity() {
			try {
				return FlightGlobals.ActiveVessel.velocityD;
			} catch(Exception) {
				UnityEngine.Debug.Log("Data acquisition failed.");
				return Vector3d.zero;
			}
		}

		[kRPC("active_vessel_altitude", GameScenes.FLIGHT)]
		public static object ActiveVesselVelocity() => PActiveVesselVelocity();

		[kRPC("active_vessel_altitude", GameScenes.FLIGHT)]
		public static double ActiveVesselAltitude() {
			try {
				return FlightGlobals.ActiveVessel.altitude;
			} catch (Exception) {
				UnityEngine.Debug.Log("Data acquisition failed.");
				return 0d;
			}
		}


	}
}
