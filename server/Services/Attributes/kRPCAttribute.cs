﻿using System;

namespace kRPC.Services.Attributes {

	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Method)]
	public class kRPCAttribute : Attribute {

		public kRPCAttribute(string name, GameScenes sceneContext) {
			_name = name;
			_sceneContext = sceneContext;
		}

		private string _name;
		private GameScenes _sceneContext;
	}
}
