﻿using kRPC.Network;
using System;

namespace kRPC {
	public interface IClient : IDisposable {
		bool Connected { get; }

		IConnection Connection { get; }

		string RemoteAddress { get; }

		void Close();

		byte[] Receive();

		int Send(byte[] buffer);
	}
}
