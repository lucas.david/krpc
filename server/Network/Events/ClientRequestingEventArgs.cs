﻿
namespace kRPC.Network.Events {
	public sealed class ClientRequestingEventArgs : ClientEventArgs {
		public byte[] Request { get; }

		public ClientRequestingEventArgs(IClient client, byte[] request) : base(client) {
			Request = request;
		}
	}
}
