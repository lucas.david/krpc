﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace kRPC.Network {
	public abstract class ClientEventArgs : EventArgs {
		public IClient Client { get; }

		protected ClientEventArgs(IClient client) {
			Client = client;
		}
	}
}
