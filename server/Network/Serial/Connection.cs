﻿#if SERIALIO_SUPPORT

using kRPC.Utils;
using System;
using System.IO.Ports;

namespace kRPC.Network.Serial {
	public class Connection : IConnection {
		private readonly SerialPort serial;
		private readonly DynamicBuffer<byte> buffer = new DynamicBuffer<byte>();

		public event EventHandler<MessageReceivedEventArgs> MessageReceived;

		public Connection(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits) {
			serial = new SerialPort(portName, baudRate, parity, dataBits, stopBits);
			serial.DataReceived += (sender, args) => {
				switch (args.EventType) {
					case SerialData.Chars:
						buffer.Reserve(buffer.Capacity() + serial.BytesToRead);
						while (serial.BytesToRead != 0) {
							int offset = buffer.Size();
							if (serial.BytesToRead < serial.ReadBufferSize) {
								buffer.Resize(buffer.Size() + serial.BytesToRead);
								serial.Read(buffer.Data(), buffer.Size(), serial.BytesToRead);
							} else {
								buffer.Resize(buffer.Size() + serial.ReadBufferSize);
								serial.Read(buffer.Data(), buffer.Size(), serial.ReadBufferSize);
							}
						}
						break;
					case SerialData.Eof:
						var messageReceivedEventArgs = new MessageReceivedEventArgs{ MessageData = buffer.ToArray() };
						buffer.Clear();
						MessageReceived?.Invoke(this, messageReceivedEventArgs);
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			};
			serial.Disposed += (sender, args) => Close();
			serial.ErrorReceived += (sender, args) => {

			};
			serial.PinChanged += (sender, args) => {

			};
		}

		public void Close() => serial.Close();

		public void Dispose() => throw new NotImplementedException();

		public void Open() => serial.Open();
		
		public void Write(DynamicBuffer<byte> data) => serial.Write(data.Data(), 0, data.Size());

	}
}

#endif
