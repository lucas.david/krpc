﻿using System;

namespace kRPC.Network {

	public class MessageReceivedEventArgs : EventArgs {
		public byte[] MessageData { get; set; }
	}
}