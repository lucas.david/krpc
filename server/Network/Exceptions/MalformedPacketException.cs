﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace kRPC.Network.Exceptions {
	public class MalformedPacketException : Exception {
		public MalformedPacketException(string message) : base(message) {}
	}
}
