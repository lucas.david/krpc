﻿using System;

namespace kRPC.Network {
	public abstract class AbstractClient : IClient, IDisposable {
		public abstract bool Connected { get; }

		public abstract string RemoteAddress { get; }

		public IConnection Connection => throw new NotImplementedException();

		protected bool disposed = false;

		protected abstract void Dispose(bool disposing);

		~AbstractClient() => Dispose(false);

		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		public abstract void Close();

		public abstract byte[] Receive();

		public abstract int Send(byte[] buffer);
	}
}
