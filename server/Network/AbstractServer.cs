﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace kRPC.Network {
	/*
	public abstract class AbstractServer : IServer {
		protected ICollection<IClient> Clients { get; } = new Collection<IClient>();

		public abstract string LocalAddress { get; }

		public bool Running { get; private set; } = false;

		public event EventHandler Opened;
		public event EventHandler<Events.ClientRequestingEventArgs> ClientRequested;
		public event EventHandler Closed;

		public AbstractServer() : base() {
			Opened += (sender, args) => Running = true;
			Closed += (sender, args) => Running = false;
		}

		protected void OnClientRequested(Events.ClientRequestingEventArgs eventArgs) => ClientRequested?.Invoke(this, eventArgs);

		protected void OnStarted(EventArgs eventArgs) => Opened?.Invoke(this, eventArgs);

		protected void OnStopped(EventArgs eventArgs) => Closed?.Invoke(this, eventArgs);

		public abstract void Start();

		public virtual void Close() {
			foreach (var client in Clients)
				client.Close();
			Clients.Clear();
			OnStopped(EventArgs.Empty);
		}

		public abstract void Update();

		public void Dispose() { }
	}

*/
}
