﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

namespace kRPC.Network.TCP {
	public class Server : IServer {
		private readonly Socket _socket;
		private readonly ICollection<IConnection> _connections;

		private readonly Thread _thread;
		private IPAddress _address;
		private long _port;
		private bool _isOpen;

		public bool IsOpen {
			get { return _isOpen; }
		}

		public string LocalAddress {
			get { return _socket.LocalEndPoint.ToString(); }
		}

		public event EventHandler Opened;
		public event EventHandler Closed;

		public Server(string address, long port) {
			if (!IPAddress.TryParse(address, out IPAddress addr))
				throw new ArgumentException("Invalid IP address.", nameof(address));
			_socket = new Socket(addr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
			_connections = new List<IConnection>();

			_thread = new Thread(() => {
				while (_isOpen) {
					try {
						_connections.Add(new Connection(_socket.Accept()));
					} catch (SocketException e) {
						Debug.LogWarningFormat("[kRPC]: TCP.Server: caught exception {0}.", e.SocketErrorCode);
					} catch (ObjectDisposedException) { } catch (Exception e) {
						Debug.LogWarningFormat("{0}: {1}.", e.GetType().Name, e.Message);
						Debug.LogWarning(e.StackTrace);
						Close();
					}
				}
			});
			_address = addr;
			_port = port;

			Opened += (sender, eventArgs) => {
				_address = ((IPEndPoint) _socket.LocalEndPoint).Address;
				_port = ((IPEndPoint) _socket.LocalEndPoint).Port;
				Debug.Log("[kRPC]: TCP.Server: started successfully.");
				Debug.Log(_address.ToString() == "0.0.0.0"
					? "[kRPC]: TCP.Server: listening on all local network interfaces."
					: "[kRPC]: TCP.Server: listening on local address.");
				Debug.LogFormat("[kRPC]: TCP.Server: listening on port {0}.", _port);
			};
			Closed += (sender, eventArgs) => Debug.Log("[kRPC]: TCP.Server closed.");
		}

		public void Close() {
			if (!_isOpen) {
				Debug.LogWarning("[kRPC]: TCP.Server: Close() requested, but server is not running.");
				return;
			}
			((IDisposable) this).Dispose();
			_thread.Join();
			Closed?.Invoke(this, EventArgs.Empty);
		}

		public void Open() {
			if (_isOpen) {
				Debug.LogWarning("[kRPC]: TCP.Server: open requested, but server is already running.");
				return;
			}
			try {
				_socket.Bind(new IPEndPoint(_address, (int) _port));
			} catch (SocketException e) {
				Debug.LogWarningFormat("[kRPC]: TCP.Server: failed to open server: socket error '{0}': {1}",
					e.SocketErrorCode, e.Message);
				return;
			}
			_isOpen = true;
			_thread.Start();
			Opened?.Invoke(this, EventArgs.Empty);
		}

		#region Disposable pattern

		~Server() {
			Dispose(false);
		}

		protected virtual void Dispose(bool disposing) {
			if (disposing)
				_socket.Close();
			_isOpen = false;
		}

		void IDisposable.Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		#endregion

	}
}