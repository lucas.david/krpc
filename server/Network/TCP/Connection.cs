﻿using Google.Protobuf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using kRPC.Network.Exceptions;
using kRPC.RPC.Protocol;
using kRPC.Utils;
using Debug = UnityEngine.Debug;

namespace kRPC.Network.TCP {
	public class Connection : IConnection {
		private readonly Socket _socket;

		private readonly Thread _thread;
		private bool _isClosed;

		private readonly byte[] _buffer;
		private readonly Queue<byte[]> _toHandle;
		private readonly Queue<byte[]> _toSend;

		public Queue<byte[]> MessagesToHandle {
			get { return _toHandle; }
		}
		public Queue<byte[]> ToSend {
			get { return _toSend; }
		}

		public string RemoteAddress {
			get {
				return _socket.RemoteEndPoint.ToString();
			}
		} 

		public event EventHandler MessageReceived;

		public Connection(Socket socket) {
			_socket = socket ?? throw new ArgumentNullException(nameof(socket), "Argument must not be null.");
			_socket.ReceiveTimeout = 5;
			_thread = new Thread(() => {
				while (!_isClosed) {
					try {
						Read();
					} finally {
						Write();
					}
				}
			});

			_toSend = new Queue<byte[]>();
			_toHandle = new Queue<byte[]>();

			_buffer = new byte[16384];

			MessageReceived += (sender, eventArgs) => {
				lock (_toHandle)
					Debug.Log(Encoding.UTF8.GetString(_toHandle.Peek()));
			};

			_isClosed = false;
			_thread.Start();
		}

		private void Read() {
			int varintSize = 1;
			while (varintSize < 12) {
				_socket.Receive(_buffer, 0, varintSize, SocketFlags.Partial);
				if (RPC.Protocol.Decoder.DecodeVarint(_buffer, out ulong packetLength)) {
					_socket.Receive(_buffer, varintSize + 1, (int)packetLength - varintSize, SocketFlags.None);
					lock (_toHandle)
						_toHandle.Enqueue(_buffer);
					MessageReceived?.Invoke(this, EventArgs.Empty);
					return;
				}
				varintSize++;
			}

			throw new MalformedPacketException("Malformed header: can't decode variable-length integer.");
		}

		private void Write() {
			var stopwatch = new Stopwatch();
			stopwatch.Start();
			while (_toSend.Count != 0 && stopwatch.ElapsedMilliseconds < 10)
				_socket.Send(_toSend.Dequeue());
			stopwatch.Stop();
		}

		public void Close() {
			if (_isClosed) {
				Debug.LogWarning("[kRPC]: TCP.Connection: Close() requested, but connection is not open.");
				return;
			}
			((IDisposable) this).Dispose();
			_thread.Join();
		}

		#region Disposable pattern

		~Connection() {
			Dispose(false);
		}

		protected virtual void Dispose(bool disposing) {
			if (disposing)
				_socket.Close();
			_isClosed = true;
		}

		void IDisposable.Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		#endregion

	}
}