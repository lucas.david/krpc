﻿using kRPC.Utils;
using System;
using System.Collections.Generic;

namespace kRPC.Network {
	public interface IConnection : IDisposable {

		Queue<byte[]> MessagesToHandle { get; }

		string RemoteAddress { get; }

		event EventHandler MessageReceived;

		void Close();

	}
}
