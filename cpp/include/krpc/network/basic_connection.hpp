#ifndef KRPC_NETWORK_CONNECTION_HPP
#define KRPC_NETWORK_CONNECTION_HPP

#include <condition_variable>
#include <queue>
#include <thread>

#include <asio/io_context.hpp>
#include <asio/connect.hpp>
#include <asio/write.hpp>
#include <asio/read.hpp>
#include <asio/ip/tcp.hpp>

#include <krpc/rpc/decoder.hpp>
#include <krpc/rpc/encoder.hpp>


namespace krpc::network {

	template<typename Protocol>
	class basic_connection {
	public:
		typedef Protocol protocol_type;
		typedef typename Protocol::endpoint endpoint_type;
		typedef typename Protocol::resolver resolver_type;
		typedef typename Protocol::resolver::results_type results_type;
		typedef typename Protocol::socket socket_type;

		typedef std::queue<std::string> message_queue;

	private:
		asio::io_context _io_context;
		resolver_type _resolver;
		socket_type _socket;
		std::thread _thread;

		std::queue<std::string> _to_write_messages;
		std::queue<std::string> _read_messages;
		bool _alive;

	public:
		/**
		 *
		 
		 */
		basic_connection() noexcept;

		/**
		 *
		 */
		~basic_connection();

		/**
		 *
		 */
		void close();

		/**
		 *
		 * @param host
		 * @param service
		 */
		void connect(const std::string &host, const std::string &service);

		bool has_read() const noexcept;

		bool is_alive() const noexcept;

		/**
		 *
		 * @param data
		 * @return
		 */
		std::string read();

		/**
		 *
		 * @param data
		 */
		void write(const std::string &data) noexcept;

		std::condition_variable &notifier() noexcept;

	private:
		/**
		 *
		 */
		void do_read() noexcept;

		/**
		 *
		 */
		void do_read_length();

		/**
		 *
		 */
		void do_read_body();

		/**
		 *
		 */
		void do_write();
	};

	template<typename Protocol>
	basic_connection<Protocol>::basic_connection() noexcept : _io_context(), _resolver(_io_context),
	                                                          _socket(_io_context),
	                                                          _thread() {}

	template<typename Protocol>
	basic_connection<Protocol>::~basic_connection() { close(); }

	template<typename Protocol>
	void basic_connection<Protocol>::close() {
		asio::post(_io_context, [this]() { _socket.close(); });
		_thread.join();
	}

	template<typename Protocol>
	void basic_connection<Protocol>::connect(const std::string &host, const std::string &service) {
		/* Resolve the host name and service to a list of endpoints. */
		results_type endpoints = _resolver.resolve(host, service);
		/* Asynchronously start connection. */
		asio::async_connect(_socket, endpoints, [this](std::error_code ec, endpoint_type) {
			if (!ec)
				do_read();
		});
		_thread = std::thread([this]() {
			_io_context.run();
		});
	}

	template<typename Protocol>
	inline bool basic_connection<Protocol>::has_read() const noexcept {
		return !_read_messages.empty();
	}

	template<typename Protocol>
	inline bool basic_connection<Protocol>::is_alive() const noexcept {
		return _thread.joinable();
	}

	template<typename Protocol>
	std::string basic_connection<Protocol>::read() {
		std::string data = _read_messages.front();
		_read_messages.pop();
		return data;
	}

	template<typename Protocol>
	void basic_connection<Protocol>::write(const std::string &data) noexcept {
		asio::post(_io_context, [this, &data]() {
			_to_write_messages.push(data);
			/* If there is no write in progress re-initiate async do_write() loop. */
			if (_to_write_messages.empty())
				do_write();
		});
		std::string
	}

	template<typename Protocol>
	void basic_connection<Protocol>::do_read() noexcept {
		_read_messages.push(std::string(1, 0));
	}

	template<typename Protocol>
	void basic_connection<Protocol>::do_read_body() {
		asio::async_read(_socket, asio::buffer(_read_messages.back(), _read_messages.back().size()),
			[this](std::error_code ec, std::size_t) {
				if (!ec) {
					/* Consumes message. */
					_notifier.notify_one();
					/* Loop. */
					do_read();
				} else
					_socket.close();
			});
	}

	template<typename Protocol>
	void basic_connection<Protocol>::do_read_length() {
		asio::async_read(_socket,
			asio::buffer(&_read_messages.back().data()[_read_messages.back().size() - 1], 1),
			[this](std::error_code ec, std::size_t) {
				/* If error or current_header length is greater than classic varint's length, close socket. */
				if (!ec or _read_messages.back().size() > 8) {
					std::size_t length = 0;
					if (rpc::decoder::decode_length(_read_messages.back(), length)) {
						_read_messages.back().resize(length);
						do_read_body();
					} else {
						_read_messages.back().resize(_read_messages.back().size() + 1);
						do_read_length();
					}
				} else
					_socket.close();
			});
	}

	template<typename Protocol>
	void basic_connection<Protocol>::do_write() {
		auto message = rpc::encoder::encode_with_length(_to_write_messages.front());
		asio::async_write(_socket, asio::buffer(message.data(), message.length()),
			[this](std::error_code ec, std::size_t) {
				if (!ec) {
					_to_write_messages.pop();
					if (!_to_write_messages.empty())
						do_write();
				} else
					_socket.close(); /* If failed. Close. */
			});
	}

	namespace ip::tcp { typedef basic_connection<asio::ip::tcp> connection; }
	/* namespace ip::udp { typedef basic_connection<asio::ip::udp> connection; } */
}

#endif
