#ifndef KRPC_ERROR_HPP
#define KRPC_ERROR_HPP

#include <stdexcept>

/**
 * Thrown when an error occurs encoding or decoding a message.
 */
class coding_error : public std::runtime_error {
protected:
	explicit coding_error(const std::string &message) : std::runtime_error(message) {}
	explicit coding_error(const char *message) : std::runtime_error(message) {}
};

#endif
