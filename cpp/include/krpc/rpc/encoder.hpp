#ifndef KRPC_RPC_ENCODER_HPP
#define KRPC_RPC_ENCODER_HPP

#include <krpc/error.hpp>
#include <krpc/rpc/protocol.hpp>


namespace proto = google::protobuf;

namespace krpc::rpc {

	template<typename T>
	class remote_object;

	namespace encoder {

		/**
		 * Thrown when an error occurs encoding a message.
		 */
		class encoding_error : public coding_error {
		public:
			explicit encoding_error(const std::string &message) noexcept : coding_error(message) {}
			explicit encoding_error(const char *message) noexcept : coding_error(message) {}
		};

		std::string encode(int value) noexcept;

		std::string encode(long value) noexcept;

		std::string encode(unsigned int value) noexcept;

		std::string encode(unsigned long value) noexcept;

		std::string encode(float value) noexcept;

		std::string encode(double value) noexcept;

		std::string encode(bool value) noexcept;

		template<typename T>
		std::string encode(const remote_object<T> &remote_object) noexcept {
			return encode(remote_object.identifier());
		}

		std::string encode(const protocol::Request &request);

		bool encode(const protocol::Request &request, std::string &data) noexcept;

		std::string encode_with_length(const std::string &data) noexcept;

		// std::string encode_with_size(const proto::Message& message);

	}
}
#endif