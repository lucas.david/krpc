#ifndef KRPC_RPC_REMOTE_OBJECT_HPP
#define KRPC_RPC_REMOTE_OBJECT_HPP

#include <cstdint>
#include <string>
#include <memory>

namespace krpc::rpc {

	class client;

	template<typename T>
	class remote_object {
	protected:
		std::shared_ptr<client> _client;

	private:
		uint64_t _identifier;
		std::string _name;

	public:
		constexpr remote_object() noexcept;
		constexpr remote_object(const std::shared_ptr<client> &remote_client, uint64_t identifier,
		                        const std::string &name) noexcept;

		std::uint64_t identifier() const noexcept;
		const std::string &name() const noexcept;

	protected:
		virtual ~remote_object() noexcept;
	};

	template<typename T>
	constexpr remote_object<T>::remote_object() noexcept : _client(), _identifier(), _name("nullptr") {}

	template<typename T>
	inline
	constexpr remote_object<T>::remote_object(const std::shared_ptr<client> &client, uint64_t identifier,
	                                          const std::string &name) noexcept : _client(client),
	                                                                              _identifier(identifier),
	                                                                              _name(name) {}

	template<typename T>
	inline remote_object<T>::~remote_object() noexcept {}

	template<typename T>
	inline std::uint64_t remote_object<T>::identifier() const noexcept {
		return this->_identifier
	}

	template<typename T>
	inline const std::string &remote_object<T>::name() const noexcept {
		return this->_name;
	}

	template<typename T>
	inline bool operator==(const remote_object<T> &lhs, const remote_object<T> &rhs) noexcept {
		return lhs.identifer() == rhs.identifier();
	}

	template<typename T>
	inline bool operator!=(const remote_object<T> &lhs, const remote_object<T> &rhs) noexcept {
		return !(lhs == rhs);
	}

	template<typename T>
	inline std::ostream &operator<<(std::ostream &os, const remote_object<T> &object) noexcept {
		return os << object.name() << '<' << object.identifier() << '>';
	}

}

#endif