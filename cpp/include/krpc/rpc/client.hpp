#ifndef KRPC_RPC_CLIENT_HPP
#define KRPC_RPC_CLIENT_HPP

#include <string>
#include <thread>
#include <condition_variable>

#include <krpc/rpc/decoder.hpp>
#include <krpc/rpc/encoder.hpp>
#include <krpc/rpc/protocol.hpp>
#include <krpc/network/basic_connection.hpp>

namespace krpc::rpc {

	class client {
	private:
		std::string _name;
		network::basic_connection<asio::ip::tcp> _connection;
		unsigned long _next_request_identifier;

		std::thread _thread;
		std::mutex _mutex;
		std::condition_variable _condition_variable;
		std::map<unsigned long, protocol::Response> _responses;

	public:
		explicit client(const std::string &name) noexcept;

		/**
		 *
		 * @param address
		 * @param service
		 */
		void connect(const std::string &address, const std::string &service);

		/**
		 * *
		 * @param procedure
		 * @return
		 */
		protocol::Response invoke(const protocol::Request::Procedure &procedure);

		/**
		 *
		 * @tparam Args
		 * @param type
		 * @param method_name
		 * @param args
		 * @return
		 */
		template<typename... Args>
		protocol::Response invoke(const std::string &type, const std::string &method_name, const Args&... args);

		/**
		 *
		 * @return
		 */
		bool is_connected() const noexcept { return _connection.is_alive(); }

	private:

		/**
		 *
		 */
		void read();

		/**
		 *
		 * @param request
		 */
		void write(const protocol::Request &request);
	};

	inline client::client(const std::string &name) noexcept : _name(name), _connection(), _next_request_identifier(1) {}

	template<typename... Args>
	inline protocol::Response
	client::invoke(const std::string &type, const std::string &method_name, const Args &... args) {
		return invoke(make_procedure(type, method_name, args...));
	}

}


#endif