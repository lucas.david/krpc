#ifndef KRPC_RPC_DECODER_HPP
#define KRPC_RPC_DECODER_HPP

#include <cstdint>

#include <google/protobuf/io/coded_stream.h>

#include <krpc/error.hpp>
#include <krpc/rpc/protocol.hpp>

namespace krpc::rpc::decoder {

	/**
	 * Thrown when an error occurs decoding data.
	 */
	class decoding_error : public coding_error {
	public:
		explicit decoding_error(const std::string &message) noexcept : coding_error(message) {}
		explicit decoding_error(const char *message) noexcept : coding_error(message) {}
	};

	/**
	 *
	 * @param data
	 * @return
	 */
	protocol::Response decode_response(const std::string &data);

	/**
	 *
	 * @param data
	 * @param response
	 * @return
	 */
	bool decode_response(const std::string &data, protocol::Response &response) noexcept;

	/**
	 *
	 * @param data
	 * @return
	 */
	std::size_t decode_length(const std::string &data);

	/**
	 *
	 * @param data
	 * @param result
	 * @return
	 */
	bool decode_length(const std::string &data, std::size_t &result) noexcept;

}

#endif