#ifndef KRPC_RPC_PROTOCOL_HPP
#define KRPC_RPC_PROTOCOL_HPP

#include <krpc/rpc/protocol.pb.hpp>

namespace proto = google::protobuf;

namespace krpc::rpc {

	template<typename... Args>
	protocol::Request::Procedure make_procedure(const std::string &type_name, const std::string &method_name, const Args&... args) {
		protocol::Request::Procedure procedure;
		procedure.set_type(type_name);
		procedure.set_name(method_name);
		(procedure.add_params()->ParseFrom(args), ...);
		return procedure;
	}

	protocol::Request make_request(unsigned long identifier, const protocol::Request::Procedure& procedure);

	protocol::Response::Error make_error(const std::string &type, const std::string &message, const std::string &stack_trace);

}

#endif