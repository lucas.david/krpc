#include <krpc/rpc/client.hpp>

namespace krpc::rpc {

	void client::connect(const std::string &address, const std::string &service) {
		_connection.connect(address, service);
		_thread = std::thread([this]() {
			std::mutex mutex;
			std::unique_lock<std::mutex> lock_guard(mutex);
			while (_connection.is_alive())
				if (_connection.has_read())
					read();
		});
	}

	protocol::Response client::invoke(const rpc::protocol::Request::Procedure &procedure) {
		unsigned long identifier = _next_request_identifier++;
		write(make_request(identifier, procedure));
		std::unique_lock<std::mutex> lock;
		_condition_variable.wait(lock, [this, &identifier]() { return _responses.count(identifier); });
		return _responses[identifier];
	}

	void client::read() {
		auto response = decoder::decode_response(_connection.read());
		_responses[static_cast<unsigned long>(response.identifier())] = response;
	}

	void client::write(const protocol::Request &request) {
		std::string data;
		encoder::encode(request, data);
		_connection.write(data);
	}

}