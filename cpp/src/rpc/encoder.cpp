#include <krpc/rpc/encoder.hpp>

#include <string>
#include <mutex>

namespace krpc::rpc::encoder {

	static const std::size_t little_endian_32_size = sizeof(proto::uint32);
	static const std::size_t little_endian_64_size = sizeof(proto::uint64);
	static std::string little_endian_32_buffer(little_endian_32_size, 0);
	static std::string little_endian_64_buffer(little_endian_64_size, 0);
	static std::string dynamic_buffer;
	static std::mutex little_endian_32_buffer_mutex;
	static std::mutex little_endian_64_buffer_mutex;
	static std::mutex dynamic_buffer_mutex;

	std::string encode(int value) noexcept {
		auto zig_zag_value = proto::internal::WireFormatLite::ZigZagEncode32(value);
		/* Ensure that the buffer is not used simultaneously by an other thread. */
		std::lock_guard<std::mutex> lock_guard(dynamic_buffer_mutex);
		dynamic_buffer.resize(proto::io::CodedOutputStream::VarintSize32(zig_zag_value));
		proto::io::CodedOutputStream::WriteVarint32ToArray(zig_zag_value,
			reinterpret_cast<proto::uint8 *>(dynamic_buffer.data()));
		return dynamic_buffer;
	}

	std::string encode(long value) noexcept {
		auto zig_zag_value = proto::internal::WireFormatLite::ZigZagEncode64(value);
		/* Ensure that the buffer is not used simultaneously by an other thread. */
		std::lock_guard<std::mutex> lock_guard(dynamic_buffer_mutex);
		dynamic_buffer.resize(proto::io::CodedOutputStream::VarintSize64(zig_zag_value));
		proto::io::CodedOutputStream::WriteVarint64ToArray(zig_zag_value,
			reinterpret_cast<proto::uint8 *>(dynamic_buffer.data()));
		return dynamic_buffer;
	}

	std::string encode(unsigned int value) noexcept {
		/* Ensure that the buffer is not used simultaneously by an other thread. */
		std::lock_guard<std::mutex> lock_guard(dynamic_buffer_mutex);
		dynamic_buffer.resize(proto::io::CodedOutputStream::VarintSize32(value));
		proto::io::CodedOutputStream::WriteVarint32ToArray(value,
			reinterpret_cast<proto::uint8 *>(dynamic_buffer.data()));
		return dynamic_buffer;
	}

	std::string encode(bool value) noexcept {
		return encode(static_cast<unsigned int>(value ? 1 : 0));
	}

	std::string encode(unsigned long value) noexcept {
		/* Ensure that the buffer is not used simultaneously by an other thread. */
		std::lock_guard<std::mutex> lock_guard(dynamic_buffer_mutex);
		dynamic_buffer.resize(proto::io::CodedOutputStream::VarintSize64(value));
		proto::io::CodedOutputStream::WriteVarint64ToArray(value,
			reinterpret_cast<proto::uint8 *>(dynamic_buffer.data()));
		return dynamic_buffer;
	}

	std::string encode(float value) noexcept {
		auto encoded_value = proto::internal::WireFormatLite::EncodeFloat(value);
		/* Ensure that the buffer is not used simultaneously by an other thread. */
		std::lock_guard<std::mutex> lock_guard(little_endian_32_buffer_mutex);
		proto::io::CodedOutputStream::WriteLittleEndian32ToArray(encoded_value,
			reinterpret_cast<proto::uint8 *>(little_endian_32_buffer.data()));
		return little_endian_32_buffer;
	}

	std::string encode(double value) noexcept {
		auto encoded_value = proto::internal::WireFormatLite::EncodeDouble(value);
		/* Ensure that the buffer is not used simultaneously by an other thread. */
		std::lock_guard<std::mutex> lock_guard(little_endian_64_buffer_mutex);
		proto::io::CodedOutputStream::WriteLittleEndian64ToArray(encoded_value,
			reinterpret_cast<proto::uint8 *>(little_endian_64_buffer.data()));
		return little_endian_64_buffer;
	}

	std::string encode(const protocol::Request &request) {
		/* Ensure that the buffer is not used simultaneously by an other thread. */
		std::lock_guard<std::mutex> lock_guard(dynamic_buffer_mutex);
		if ((dynamic_buffer = request.SerializeAsString()).empty())
			throw encoding_error("");
		return dynamic_buffer;
	}

	bool encode(const protocol::Request &request, std::string &data) noexcept {
		return request.SerializeToString(&data);
	}

	std::string encode_with_length(const std::string &data) noexcept {
		/* Ensure that the buffer is not used simultaneously by an other thread. */
		std::lock_guard<std::mutex> lock_guard(dynamic_buffer_mutex);
		dynamic_buffer.resize(proto::io::CodedOutputStream::VarintSize64(data.size()));
		proto::io::CodedOutputStream::WriteVarint64ToArray(data.size(),
			reinterpret_cast<proto::uint8 *>(dynamic_buffer.data()));
		return dynamic_buffer += data;
	}

	/*
	std::string encode_with_size(const proto::Message &message) {
		auto size = message.ByteSizeLong();
		*//* Ensure that the buffer is not used simultaneously by an other thread. *//*
		std::lock_guard<std::mutex> lock_guard(dynamic_buffer_mutex);
		dynamic_buffer.resize(proto::io::CodedOutputStream::VarintSize64(size));
		proto::io::CodedOutputStream::WriteVarint64ToArray(size,
		                                                   reinterpret_cast<proto::uint8 *>(dynamic_buffer.data()));
		return dynamic_buffer += message.SerializeAsString();
	}
	*/

}