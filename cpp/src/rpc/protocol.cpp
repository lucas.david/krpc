#include <krpc/rpc/protocol.hpp>

#include <krpc/rpc/decoder.hpp>

namespace krpc::rpc {

	protocol::Request make_request(unsigned long identifier, const protocol::Request::Procedure &procedure) {
		protocol::Request request;
		request.set_identifier(identifier);
		*request.mutable_procedure() = procedure;
		return request;
	}

	protocol::Response::Error make_error(const std::string &type, const std::string &message, const std::string &stack_trace) {
		protocol::Response::Error error;
		error.set_type(type);
		error.set_message(message);
		error.set_stack_trace(stack_trace);
		return error;
	}


}