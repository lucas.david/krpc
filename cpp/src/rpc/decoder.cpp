#include <krpc/rpc/decoder.hpp>


namespace krpc::rpc::decoder {

	namespace proto = google::protobuf;

	protocol::Response decode_response(const std::string &data) {
		protocol::Response response;
		if (!response.ParseFromString(data))
			throw decoding_error("Failed to decode response.");
		return response;
	}

	bool decode_response(const std::string &data, protocol::Response &response) noexcept {
		return response.ParseFromString(data);
	}

	std::size_t decode_length(const std::string &buffer) {
		std::size_t result;
		proto::io::CodedInputStream stream(reinterpret_cast<const proto::uint8*>(buffer.data()),
			static_cast<int>(buffer.size()));
		if (!stream.ReadVarint32(reinterpret_cast<google::protobuf::uint32 *>(&result)))
			throw decoding_error("Failed to decode lenght.");
		return result;
	}

	bool decode_length(const std::string &buffer, std::size_t &result) noexcept {
		proto::io::CodedInputStream stream(reinterpret_cast<const proto::uint8*>(buffer.data()),
			static_cast<int>(buffer.size()));
		return stream.ReadVarint32(reinterpret_cast<google::protobuf::uint32 *>(&result));
	}

}