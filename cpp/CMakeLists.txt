cmake_minimum_required(VERSION 3.12)
project(krpc VERSION 0.0.1 DESCRIPTION "kRPC: Remote Procedure Call library for Kerbal Space Program" LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)

if (WIN32) # Manual library installation
    # add_definitions(-D_WIN32_WINNT=0x0601)
    # Protocole Buffers compilation informations.
    include_directories(libs/protobuf-3.8.0/include)
    link_directories(libs/protobuf-3.8.0/lib)
endif()

# Asio header library informations (manual installation).
include_directories(libs/asio-1.12.2/include)
add_definitions(-DASIO_STANDALONE)

include_directories(include)
file(GLOB_RECURSE source_files src/* include/*)

add_library(krpc SHARED ${source_files})
add_executable(krpc-test test/main.cpp ${source_files})

# generates the export header shared_exports.h automatically for shared library compability. Linux only soon ?
# include(GenerateExportHeader)
# generate_export_header(cpp)

target_link_libraries(krpc libs/protobuf-3.8.0/lib/libprotobuf.lib)
target_link_libraries(krpc-test libs/protobuf-3.8.0/lib/libprotobuf.lib)
