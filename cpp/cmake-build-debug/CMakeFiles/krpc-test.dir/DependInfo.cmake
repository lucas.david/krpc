# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/Users/ldavid/Desktop/krpc/cpp/src/krpc.cpp" "C:/Users/ldavid/Desktop/krpc/cpp/cmake-build-debug/CMakeFiles/krpc-test.dir/src/krpc.cpp.obj"
  "C:/Users/ldavid/Desktop/krpc/cpp/src/rpc/client.cpp" "C:/Users/ldavid/Desktop/krpc/cpp/cmake-build-debug/CMakeFiles/krpc-test.dir/src/rpc/client.cpp.obj"
  "C:/Users/ldavid/Desktop/krpc/cpp/src/rpc/decoder.cpp" "C:/Users/ldavid/Desktop/krpc/cpp/cmake-build-debug/CMakeFiles/krpc-test.dir/src/rpc/decoder.cpp.obj"
  "C:/Users/ldavid/Desktop/krpc/cpp/src/rpc/encoder.cpp" "C:/Users/ldavid/Desktop/krpc/cpp/cmake-build-debug/CMakeFiles/krpc-test.dir/src/rpc/encoder.cpp.obj"
  "C:/Users/ldavid/Desktop/krpc/cpp/src/rpc/protocol.cpp" "C:/Users/ldavid/Desktop/krpc/cpp/cmake-build-debug/CMakeFiles/krpc-test.dir/src/rpc/protocol.cpp.obj"
  "C:/Users/ldavid/Desktop/krpc/cpp/src/rpc/protocol.pb.cpp" "C:/Users/ldavid/Desktop/krpc/cpp/cmake-build-debug/CMakeFiles/krpc-test.dir/src/rpc/protocol.pb.cpp.obj"
  "C:/Users/ldavid/Desktop/krpc/cpp/test/main.cpp" "C:/Users/ldavid/Desktop/krpc/cpp/cmake-build-debug/CMakeFiles/krpc-test.dir/test/main.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ASIO_STANDALONE"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../libs/protobuf-3.8.0/include"
  "../libs/asio-1.12.2/include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
